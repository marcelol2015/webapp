package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.Certificado;
import com.acepta.ecuador.persistence.Solicitud;
import com.acepta.ecuador.persistence.SolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Modulo:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Obtiene Solicitud(es) por Numero de Solicitud, Numero de Serie de Certificado, o por Estado.
 *   
 */
public class ObtieneSolicitudes extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String estado = request.getParameter("Estado");
		String numeroSerie = request.getParameter("NumeroSerie");
		String numeroSolicitud = request.getParameter("NumeroSolicitud");

		obtieneSolicitudes(request, response, estado, numeroSerie, numeroSolicitud);
	}
	
	private void obtieneSolicitudes(HttpServletRequest request, HttpServletResponse response, String estado, String numeroSerie, String numeroSolicitud) throws IOException {
		try {
			Collection<SolicitudPersona> solicitudes = findSolicitudes(estado, numeroSerie, numeroSolicitud);
	
			sendSolicitudes(request, response, solicitudes);
			
			log("[Acepta] Se encontraron " + solicitudes.size() + " Solicitudes " + (estado != null ? "en estado " + formatearEstado(Integer.parseInt(estado)) : numeroSerie != null ? "con Numero Serie: " + numeroSerie : "con Numero Solicitud: " + numeroSolicitud));
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al buscar Solicitudes", exception);
		}
	}

	private Collection<SolicitudPersona> findSolicitudes(String estado, String numeroSerie, String numeroSolicitud) throws ServletException {
		if (estado != null) {
			return findSolicitudesByEstado(estado);
		}
		else if (numeroSerie != null) {
			return findSolicitudesByNumeroSerie(numeroSerie);
		}
		else if (numeroSolicitud != null) {
			return findSolicitudesByNumeroSolicitud(numeroSolicitud);
		}
		else {
			throw new ServletException("No se especifico ningun parametro de busqueda");
		}
	}

	private Collection<SolicitudPersona> findSolicitudesByNumeroSolicitud(String numero) throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return Arrays.asList(personaBean.buscarSolicitudPersonaConNumero(numero));
		}
		finally {
			personaBean.close();
		}
	}

	private Collection<SolicitudPersona> findSolicitudesByNumeroSerie(String numeroSerie) throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.buscarSolicitudesPersonaConNumeroSerie(new BigInteger(numeroSerie, 10));
		}
		finally {
			personaBean.close();
		}
	}

	private Collection<SolicitudPersona> findSolicitudesByEstado(String estado) throws ServletException {
		switch (Integer.parseInt(estado)) {
		case SOLICITUD_ESTADO_REGISTRADO:
		case SOLICITUD_ESTADO_VALIDADO:
		case SOLICITUD_ESTADO_RECHAZADO:
		case SOLICITUD_ESTADO_ANULADO:			
			return findSolicitudesByEstadoSolicitud(Integer.parseInt(estado));
			
		case SOLICITUD_ESTADO_EMITIDO:
		case SOLICITUD_ESTADO_SUSPENDIDO:
		case SOLICITUD_ESTADO_REVOCADO:
			return findSolicitudesByEstadoCertificado(Integer.parseInt(estado));
			
		default:
			throw new ServletException("Estado desconocido: " + estado);
		}
	}

	private Collection<SolicitudPersona> findSolicitudesByEstadoSolicitud(int estado) {
		Solicitud.Estado estadoSolicitud = null;
		
		switch (estado) {
		case SOLICITUD_ESTADO_REGISTRADO:
			estadoSolicitud = Solicitud.Estado.PENDIENTE;
			break;
		
		case SOLICITUD_ESTADO_VALIDADO:
			estadoSolicitud = Solicitud.Estado.APROBADA;
			break;
		
		case SOLICITUD_ESTADO_RECHAZADO:
			estadoSolicitud = Solicitud.Estado.RECHAZADA;
			break;
			
		case SOLICITUD_ESTADO_ANULADO:
			estadoSolicitud = Solicitud.Estado.ANULADA;
			break;			
		}
		
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.buscarSolicitudesPersonaConEstado(estadoSolicitud);
		}
		finally {
			personaBean.close();
		}
	}

	private Collection<SolicitudPersona> findSolicitudesByEstadoCertificado(int estado) {
		Certificado.Estado estadoCertificado = null;
		
		switch (estado) {
		case SOLICITUD_ESTADO_EMITIDO:
			estadoCertificado = Certificado.Estado.VALIDO;
			break;
		
		case SOLICITUD_ESTADO_SUSPENDIDO:
		case SOLICITUD_ESTADO_REVOCADO:
			estadoCertificado = Certificado.Estado.REVOCADO;
			break;
		}

		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			Collection<SolicitudPersona> solicitudes = personaBean.buscarSolicitudesPersonaConEstadoCertificado(estadoCertificado);
		
			if (estado == SOLICITUD_ESTADO_SUSPENDIDO) {
				ArrayList<SolicitudPersona> solicitudesSuspendidas = new ArrayList<SolicitudPersona>();
			
				for (SolicitudPersona solicitud : solicitudes) {
					if (solicitud.getCertificado().isSuspendido())
						solicitudesSuspendidas.add(solicitud);
				}
			
				solicitudes = solicitudesSuspendidas;
			}
		
			return solicitudes;
		}
		finally {
			personaBean.close();
		}
	}

	private void sendSolicitudes(HttpServletRequest request, HttpServletResponse response, Collection<SolicitudPersona> solicitudes) throws IOException, ServletException {
		// TODO: mejorar forma de obtener URL del servlet GetXML
		String getxmlURL = request.getRequestURL().toString().replace("obtienesolicitudes", "getxml");
		
		response.setContentType("text/xml");
		
		PrintWriter writer = response.getWriter();
		try {
			writer.println("<?xml version=\"1.0\" encoding=\"" + response.getCharacterEncoding() + "\" ?>");
			writer.println("<Solicitudes>");

			for (SolicitudPersona solicitud: solicitudes) {
				sendSolicitud(writer, solicitud, getxmlURL);
			}
			
			writer.println("</Solicitudes>");
		}
		finally {
			writer.close();
		}
	}

	private void sendSolicitud(PrintWriter writer, SolicitudPersona solicitud, String getxmlURL) throws ServletException	{
		String custodiumURL = getxmlURL + "?FileName=" + solicitud.getCodigo() + "&Basura=/v01/3141592653589793131415926535897931314159?k=31415926535897931314159265358979";

		writer.println("<Solicitud>");
		writer.println("<NSolicitud>" + solicitud.getNumero() + "</NSolicitud>");
		writer.println("<DNI>" + solicitud.getTitular().getDni() + "</DNI>");
		writer.println("<Email>" + solicitud.getTitular().getCorreo() + "</Email>");
		writer.println("<Fecha>" + dateFormat.format(solicitud.getFecha()) + "</Fecha>");
		writer.println("<ApellidoPaterno>" + solicitud.getTitular().getApellidoPaterno() + "</ApellidoPaterno>");
		writer.println("<ApellidoMaterno>" + solicitud.getTitular().getApellidoMaterno() + "</ApellidoMaterno>");
		writer.println("<Nombres>" + solicitud.getTitular().getNombres() + "</Nombres>");
		writer.println("<Profesion>" + solicitud.getTitular().getProfesion() + "</Profesion>");
		writer.println("<Pagado>" + solicitud.isPagada() + "</Pagado>");
		writer.println("<MontoPagado>" + solicitud.getMontoPagado() + "</MontoPagado>");
		writer.println("<Duracion>" + solicitud.getMesesVigencia() + "</Duracion>");
		writer.println("<XML>" + encryptData(custodiumURL) + "</XML>");
		writer.println("<IDTitular>0</IDTitular>");
		writer.println("<IDCertificadoBase>0</IDCertificadoBase>");
		writer.println("<CodSolicitud>" + solicitud.getCodigo() + "</CodSolicitud>");
		writer.println("</Solicitud>");
	}
}
