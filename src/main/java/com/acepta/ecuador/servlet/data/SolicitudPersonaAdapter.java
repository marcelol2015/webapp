package com.acepta.ecuador.servlet.data;

import com.acepta.ecuador.persistence.Certificado;
import com.acepta.ecuador.persistence.Persona;
import com.acepta.ecuador.persistence.SolicitudPersona;

import java.util.Date;


public class SolicitudPersonaAdapter implements SolicitudPersonaData {
	private SolicitudPersona solicitud;
	public SolicitudPersonaAdapter(SolicitudPersona solicitud){
		this.solicitud = solicitud;
	}
	@Override
	public Persona getTitular() {
		return solicitud.getTitular();
	}
	@Override
	public void setTitular(Persona titular) {
		solicitud.setTitular(titular);
	}
	@Override
	public Certificado getCertificado() {
		return solicitud.getCertificado();
	}
	@Override
	public void setCertificado(Certificado certificado) {
		solicitud.setCertificado(certificado);
	}
	@Override
	public Date getFecha() {
		return solicitud.getFecha();
	}
	@Override
	public SolicitudPersona getSolicitudPersona() {
		return this.solicitud;
	}
	@Override
	public void setSolicitudPersona(SolicitudPersona solicitudPersona) {
		this.solicitud = solicitudPersona;
	}


}
