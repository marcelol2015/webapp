package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.persistence.Certificado;
import org.apache.velocity.VelocityContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Modulo:
 * 	???
 * 
 * Descripcion:
 * 	Listado de Certificados emitidos por Acepta.com S.A.
 *
 */
public class ListaCertificados extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String VM_LISTA_CERTIFICADOS = "/META-INF/vm/ListaCertificados.vm";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Map<String, Collection<Certificado>> certificados = findCertificadosEmitidos();
	
			sendCertificadosEmitidos(request, response, certificados);
			
			log("[Acepta] Listado de Certificados emitidos enviado");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al enviar listado de Certificados emitidos", exception);
		}
	}

	private Map<String, Collection<Certificado>> findCertificadosEmitidos() throws ServletException {
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		try {
			Map<String, Collection<Certificado>> certificados = new HashMap<String, Collection<Certificado>>();
			
			for (String nombreAutoridad : certificadoBean.getNombresAutoridadesCertificadoras()) {
				certificados.put(nombreAutoridad, certificadoBean.buscarCertificadosConEmisor(nombreAutoridad));
			}
			
			return certificados;
		}
		finally {
			certificadoBean.close();
		}
	}
	
	private void sendCertificadosEmitidos(HttpServletRequest request, HttpServletResponse response, Map<String, Collection<Certificado>> certificados) throws IOException, ServletException {
		VelocityContext context = new VelocityContext();
		
		context.put("fecha", new SimpleDateFormat("yyyy-MM-dd"));
		context.put("certificados", certificados);

		mergeVelocityTemplate("text/html", VM_LISTA_CERTIFICADOS, context, response);
	}	
}
