package com.acepta.ecuador.servlet;

import com.acepta.ecuador.security.util.PKCS10;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Modulo:
 *  Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Devuelve nombre del Titular de un PKCS #10.
 * 
 */
public class CheckPKCS10 extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String pkcs10Base64 = request.getParameter("Solicitud");

			String subjectName = getSubjectNameFromPKCS10(pkcs10Base64);

			sendSubjectName(response, subjectName);
			
			log("[Acepta] Titular del PKCS #10 es " + subjectName);
		}
		catch (Exception exception) {
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);

			log("[Acepta] Falla al extraer Titular del PKCS #10", exception);
		}
	}

	private void sendSubjectName(HttpServletResponse response, String subjectName) throws IOException {
		response.setContentType("text/plain");

		PrintWriter writer = response.getWriter();
		try {
			writer.println(subjectName);
		}
		finally {
			writer.close();
		}
	}

	private String getSubjectNameFromPKCS10(String pkcs10Base64) {
		return PKCS10.decodeSubjectFromPKCS10CertificateRequest(pkcs10Base64);
	}
}
