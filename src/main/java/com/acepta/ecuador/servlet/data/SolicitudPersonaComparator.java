package com.acepta.ecuador.servlet.data;

import com.acepta.ecuador.persistence.Certificado;

import java.util.Comparator;

public class SolicitudPersonaComparator implements Comparator<SolicitudPersonaData>{

	public int compare(SolicitudPersonaData s1, SolicitudPersonaData s2) {
		Certificado cer1 = s1.getCertificado();
		Certificado cer2 = s2.getCertificado();
		if ((cer1==null) && (cer2==null))
		   return 0;
		else if ((cer1==null) && (cer2!=null) )
		   return 1;
		else if ((cer1!=null) && (cer2==null))
			return -1;
		else
		  return cer1.getValidoDesde().compareTo(cer2.getValidoDesde());
	}
}
