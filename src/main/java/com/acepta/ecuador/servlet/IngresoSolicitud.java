package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.SolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Modulo:
 * 	Aplicacion Operador Registro.
 * 
 * Descripcion:
 * 	Ingresa una nueva Solicitud.
 *
 */
public class IngresoSolicitud extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			SolicitudPersona solicitud = ingresarSolicitud(getSolicitudFromPUTRequest(request));
	
			sendText(response, "SOLICITUD No. " + solicitud.getCodigo() + " INGRESADA");
		
			log("[Acepta] Solicitud No. " + solicitud.getCodigo() + " ingresada (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronica") + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());

			log("[Acepta] Falla al ingresar Solicitud", exception);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			SolicitudPersona solicitud = ingresarSolicitud(getSolicitudFromPOSTRequest(request));
	
			sendText(response, "SOLICITUD No. " + solicitud.getCodigo() + " INGRESADA");
		
			log("[Acepta] Solicitud No. " + solicitud.getCodigo() + " ingresada (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronica") + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());

			log("[Acepta] Falla al ingresar Solicitud", exception);
		}
	}

	private SolicitudPersona ingresarSolicitud(byte[] data) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.ingresarSolicitudPersona(data);
		}
		finally {
			personaBean.close();
		}
	}
	
	private byte[] getSolicitudFromPOSTRequest(HttpServletRequest request) throws ServletException {
		try {
			String solicitudXml = request.getParameter("Solicitud");
			
			if (solicitudXml == null)
				throw new ServletException("No se entrego ninguna Solicitud");
			
			return createXMLDataFromString(solicitudXml);
		}
		catch (UnsupportedEncodingException exception) {
			throw new ServletException("Falla al decodificar Solicitud", exception);
		}
	}
	
	private static byte[] createXMLDataFromString(String document) throws UnsupportedEncodingException {
		String encoding = "UTF-8";
		
		if (document.startsWith("<?xml ")) {
			String declaration = document.substring(0, document.indexOf("?>") + 2);
			
			if (declaration.contains("ISO-8859-1"))
				encoding = "ISO-8859-1";
			
			if (!declaration.contains(encoding))
				throw new UnsupportedEncodingException();
		}
		
		return document.getBytes(encoding);
	}
	
	private byte[] getSolicitudFromPUTRequest(HttpServletRequest request) throws ServletException, IOException {
		if (request.getContentType() == null || !request.getContentType().equals("text/xml"))
			throw new ServletException("Solo se aceptan Solicitudes en formato XML");
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = request.getInputStream();
			try {
				byte[] buffer = new byte[1024];
				int length;
				
				while ((length = input.read(buffer)) > 0)
					output.write(buffer, 0, length);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}

}
