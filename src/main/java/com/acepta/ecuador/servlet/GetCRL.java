package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanException;
import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.persistence.CRL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Devuelve las ultimas CRLs de las Autoridades Certificadoras.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2007-10-02
 *
 */
public class GetCRL extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String CRL_MIME_TYPE = "application/pkix-crl";
	private static final String CRL_FILE_EXTENSION = ".crl";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String nombreAutoridad = getNombreAutoridadFromRequest(request);
	
			CRL crl = findLatestCRLWithEmisor(nombreAutoridad);
	
			if (crl != null) {
				sendCRL(response, crl);
				
				log("[Acepta] CRL enviada (Autoridad: " + nombreAutoridad + ")");
			}
			else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				
				log("[Acepta] No existe la CRL de la Autoridad: " + nombreAutoridad);
			}
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al devolver CRL", exception);
		}
	}
	
	private void sendCRL(HttpServletResponse response, CRL crl) throws IOException {
		byte[] data = crl.getData();
		
		response.setContentType(CRL_MIME_TYPE);
		response.setContentLength(data.length);
		
		response.setDateHeader("Expires", crl.getValidoHasta().getTime());
		
		OutputStream output = response.getOutputStream();
		try {
			output.write(data);
		}
		finally {
			output.close();
		}
	}
	
	private String getNombreAutoridadFromRequest(HttpServletRequest request) throws ServletException {
		String pathInfo = request.getPathInfo();
		
		if (pathInfo.startsWith("/") && pathInfo.endsWith(CRL_FILE_EXTENSION))
			return pathInfo.substring(1, pathInfo.length() - CRL_FILE_EXTENSION.length());
		
		throw new ServletException("Peticion de CRL invalida: " + pathInfo);
	}
	
	private CRL findLatestCRLWithEmisor(String nombreEmisor) throws ServletException {
		try {
			CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
			try {
				if (certificadoBean.getNombresAutoridadesCertificadoras().contains(nombreEmisor))
					return certificadoBean.buscarUltimaCRLConEmisor(nombreEmisor);
			}
			finally {
				certificadoBean.close();
			}
			
			return null;
		}
		catch (AceptaBeanException exception) {
			throw new ServletException("Falla al buscar la CRL de la Autoridad: " + nombreEmisor, exception);
		}
	}
}
