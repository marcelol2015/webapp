package com.acepta.ecuador.servlet;

import com.acepta.ecuador.xml.XMLSolicitudPersona;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.ParameterParser;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class CambiaEstadoForm {
	private static final String CONTENT_TYPE_CHARSET_PARAMETER_NAME = "charset";
	private static final String CONTENT_TYPE_CHARSET_DEFAULT = "ISO-8859-1";
	private static final char CONTENT_TYPE_SEPARATOR = ';';
	
	private int estado;
	private String comentario;
	private byte[] xml;
	
	public CambiaEstadoForm(HttpServletRequest request) throws ServletException {
		parseForm(request);
		verifyForm();
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public byte[] getData() {
		return xml;
	}

	public void setData(byte[] xml) {
		this.xml = xml;
	}

	public XMLSolicitudPersona getSolicitudPersona() throws SAXException, IOException {
		ByteArrayInputStream input = new ByteArrayInputStream(getData());
		try {
			return new XMLSolicitudPersona(input);
		}
		finally {
			input.close();
		}
	}
	
	private void parseForm(HttpServletRequest request) throws ServletException {
		if (!ServletFileUpload.isMultipartContent(request))
			throw new ServletException("Formulario no es multiparte");
		
		try {
			ServletFileUpload fileUpload = new ServletFileUpload();
			
			FileItemIterator fileItemIterator = fileUpload.getItemIterator(request);
			while (fileItemIterator.hasNext()) {
				FileItemStream item = fileItemIterator.next();
			
				String fieldName = item.getFieldName();
				
				if (item.isFormField()) {
					String fieldValue = readFormField(item);
					
					if (fieldName.equals("Estado")) {
						setEstado(Integer.parseInt(fieldValue));
					}
					else if (fieldName.equals("Comentario")) {
						setComentario(fieldValue);
					}
					else if (fieldName.equals("XML")) {
						setData(fieldValue.getBytes(CONTENT_TYPE_CHARSET_DEFAULT));
					}
				}
				else {
					if (fieldName.equals("XML")) {
						setData(readFormFile(item));
					}
				}
			}
		}
		catch (FileUploadException exception) {
			throw new ServletException("Formulario invalido", exception);
		}
		catch (NumberFormatException exception) {
			throw new ServletException("Estado invalido", exception);
		}
		catch (IOException exception) {
			throw new ServletException("Falla al leer el formulario", exception);
		}
	}

	private static String readFormField(FileItemStream item) throws IOException {
		return new String(getFormRawData(item), getFormCharSet(item));
	}

	private static byte[] readFormFile(FileItemStream item) throws IOException {
		return getFormRawData(item);
	}

	private static byte[] getFormRawData(FileItemStream item) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = item.openStream();
			try {
				byte[] buffer = new byte[1024];
				int length;
				
				while ((length = input.read(buffer)) > 0)
					output.write(buffer, 0, length);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		return output.toByteArray();
	}

	@SuppressWarnings("unchecked")
	private static String getFormCharSet(FileItemStream item) {
		ParameterParser parser = new ParameterParser();
		parser.setLowerCaseNames(true);
		
		Map<String, String> contentType = parser.parse(item.getContentType(), CONTENT_TYPE_SEPARATOR);
		
		String charset = contentType.get(CONTENT_TYPE_CHARSET_PARAMETER_NAME);
		if (charset == null)
			charset = CONTENT_TYPE_CHARSET_DEFAULT;

		return charset;
	}
	
	private void verifyForm() throws ServletException {
		if (getComentario() == null || getData() == null)
			throw new ServletException("Falta informacion en el formulario");
	}
}
