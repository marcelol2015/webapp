package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanFactory;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import javax.mail.Session;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class AceptaServletContextListener implements ServletContextListener {
	/*
	 * Servlet Context Attributes
	 */
	public static final String ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME = "AceptaBeanFactory";
	public static final String ACEPTA_BEAN_TIMER_ATTRIBUTE_NAME = "AceptaBeanTimer";
	public static final String VELOCITY_ENGINE_ATTRIBUTE_NAME = "VelocityEngine";
	public static final String MAIL_SESSION_ATTRIBUTE_NAME = "MailSession";
	public static final String LEGACY_ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME = "LegacyAceptaBeanFactory";
	public static final String SUPPORTED_TOKEN_PROVIDERS_ATTRIBUTE_NAME = "SupportedTokenProviders";
	public static final String SUPPORTED_TOKEN_PRODUCTS_ATTRIBUTE_NAME = "SupportedTokenProducts";	
	
	/*
	 * Servlet Context Parameters
	 */
	private static final String ACEPTA_CA_PERSISTENCE_UNIT_NAME_PROPERTY_NAME = "com.aceptaecuador.ca.persistenceUnitName";
	private static final String ACEPTA_CA_SECURITY_TOKEN_NAME_PROPERTY_NAME = "com.aceptaecuador.ca.securityTokenName";
	private static final String ACEPTA_CA_MAIL_SERVER_PROPERTY_NAME = "com.acepta.ca.mailServer";
		private static final String ACEPTA_CA_SUPPORTED_TOKEN_PROVIDERS_PROPERTY_NAME = "com.acepta.ca.supportedTokenProviders";
	private static final String ACEPTA_CA_SUPPORTED_TOKEN_PRODUCTS_PROPERTY_NAME = "com.acepta.ca.supportedTokenProducts";	

	/*
	 * Default Hardware Token Providers
	 */
	private static final String ACEPTA_CA_SUPPORTED_TOKEN_PROVIDERS_DEFAULT_VALUE = "eToken Base Cryptographic Provider, FTSafe ePass2000 RSA Cryptographic Service Provider";
	
	/*
	 * Default Hardware Tokens Products
	 */
	private static final String ACEPTA_CA_SUPPORTED_TOKENS_PRODUCTS_DEFAULT_VALUE="eToken Base Cryptographic Provider(eToken Pro 16K[0529:050C];eToken Pro 32K[0529:0514];eToken Pro 72K[0529:0620]),FTSafe ePass2000 RSA Cryptographic Service Provider(ePass 2000[096E:0005])"; 
	
	private AceptaBeanFactory beanFactory;
	private AceptaServletTimer beanTimer;
	private VelocityEngine velocityEngine;
	private Session mailSession;
	private List<String> supportedTokenProviders;
	private List<String> supportedTokenProducts;
	
	public AceptaServletContextListener() {
	}
	
	public AceptaBeanFactory getBeanFactory() {
		return beanFactory;
	}

	public void setBeanFactory(AceptaBeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}

	public AceptaServletTimer getBeanTimer() {
		return beanTimer;
	}

	public void setBeanTimer(AceptaServletTimer beanTimer) {
		this.beanTimer = beanTimer;
	}
	
	public VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public Session getMailSession() {
		return mailSession;
	}

	public void setMailSession(Session mailSession) {
		this.mailSession = mailSession;
	}

	public List<String> getSupportedTokenProviders() {
		return supportedTokenProviders;
	}
	public List<String> getSupportedTokenProducts() {
		return this.supportedTokenProducts;
	}
	public void setSupportedTokenProviders(List<String> supportedTokenProviders) {
		this.supportedTokenProviders = supportedTokenProviders;
	}
	public void setSupportedTokenProducts(List<String> supportedTokenProducts){
		this.supportedTokenProducts = supportedTokenProducts;
	}
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		
		setBeanFactory(createBeanFactory(context));
		
		setBeanTimer(createBeanTimer(context));
		
		setVelocityEngine(createVelocityEngine(context));
		
		setMailSession(createMailSession(context));
		
		setSupportedTokenProviders(createSupportedTokenProviders(context));
		
		setSupportedTokenProducts(createSupportedTokenProducts(context));
		
		context.setAttribute(ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME, getBeanFactory());
		context.setAttribute(ACEPTA_BEAN_TIMER_ATTRIBUTE_NAME, getBeanTimer());
		context.setAttribute(VELOCITY_ENGINE_ATTRIBUTE_NAME, getVelocityEngine());
		context.setAttribute(MAIL_SESSION_ATTRIBUTE_NAME, getMailSession());
		context.setAttribute(SUPPORTED_TOKEN_PROVIDERS_ATTRIBUTE_NAME, getSupportedTokenProviders());
		context.setAttribute(SUPPORTED_TOKEN_PRODUCTS_ATTRIBUTE_NAME, getSupportedTokenProducts());
		
		getBeanTimer().start();
		
		context.log("[Acepta] Contexto inicializado");
	}

	public void contextDestroyed(ServletContextEvent event) {
		ServletContext context = event.getServletContext();

		context.removeAttribute(SUPPORTED_TOKEN_PROVIDERS_ATTRIBUTE_NAME);
		context.removeAttribute(SUPPORTED_TOKEN_PRODUCTS_ATTRIBUTE_NAME);		
		context.removeAttribute(LEGACY_ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME);
		context.removeAttribute(MAIL_SESSION_ATTRIBUTE_NAME);
		context.removeAttribute(VELOCITY_ENGINE_ATTRIBUTE_NAME);
		context.removeAttribute(ACEPTA_BEAN_TIMER_ATTRIBUTE_NAME);
		context.removeAttribute(ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME);
		
		
		if (getMailSession() != null)
			setMailSession(null);
		
		if (getVelocityEngine() != null)
			setVelocityEngine(null);

		if (getBeanTimer() != null) {
			getBeanTimer().stop();
			setBeanTimer(null);
		}
		
		if (getBeanFactory() != null) {
			getBeanFactory().close();
			setBeanFactory(null);
		}
		
		context.log("[Acepta] Contexto destruido");
	}
	
	private AceptaBeanFactory createBeanFactory(ServletContext context) {
		String persistenceUnitName = context.getInitParameter(ACEPTA_CA_PERSISTENCE_UNIT_NAME_PROPERTY_NAME);
		String securityTokenName = context.getInitParameter(ACEPTA_CA_SECURITY_TOKEN_NAME_PROPERTY_NAME);
		
		if (persistenceUnitName == null)
			persistenceUnitName = System.getProperty(ACEPTA_CA_PERSISTENCE_UNIT_NAME_PROPERTY_NAME);
		
		if (securityTokenName == null)
			securityTokenName = System.getProperty(ACEPTA_CA_SECURITY_TOKEN_NAME_PROPERTY_NAME);
		
		return new AceptaBeanFactory(persistenceUnitName, securityTokenName);
	}
	
	private AceptaServletTimer createBeanTimer(ServletContext context) {
		AceptaServletTimer timer = new AceptaServletTimer();

		GenerarCRL generarCRL = new GenerarCRL();
		
		generarCRL.setServletContext(context);
		
		timer.schedule(generarCRL, generarCRL.getTimerPeriod());

		AvisoExpiracion avisoExpiracion = new AvisoExpiracion();
		
		avisoExpiracion.setServletContext(context);
		
		timer.schedule(avisoExpiracion, avisoExpiracion.getTimerPeriod());
		
		return timer;
	}
	
	private VelocityEngine createVelocityEngine(ServletContext context) {
		VelocityEngine ve = new VelocityEngine();
		
		ve.setProperty(VelocityEngine.INPUT_ENCODING, "UTF-8");
		ve.setProperty(VelocityEngine.OUTPUT_ENCODING, "UTF-8");
		
		ve.setProperty(VelocityEngine.RESOURCE_LOADER, "classpath");
		
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		ve.setProperty("classpath.resource.loader.cache", "true");
		
		try {
			ve.init();
		}
		catch (Exception exception) {
			throw new RuntimeException(exception);
		}
		
		return ve;
	}
	
	private Session createMailSession(ServletContext context) {
		try {
			String server = context.getInitParameter(ACEPTA_CA_MAIL_SERVER_PROPERTY_NAME);
		
			if (!server.startsWith("smtp://"))
				throw new MalformedURLException("Unknown mail transport protocol: " + server);
			
			Properties properties = new Properties();
		
			properties.put("mail.transport.protocol", server.substring(0, server.indexOf("://")));
			properties.put("mail.smtp.host", server.substring(server.indexOf("://") + 3));
			properties.put("mail.smtp.connectiontimeout", "60000");
			properties.put("mail.smtp.timeout", "30000");

			return Session.getInstance(properties);
		}
		catch (MalformedURLException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	private List<String> createSupportedTokenProviders(ServletContext context) {
		String supportedTokenProviders = context.getInitParameter(ACEPTA_CA_SUPPORTED_TOKEN_PROVIDERS_PROPERTY_NAME);
		
		if (supportedTokenProviders == null)
			supportedTokenProviders = ACEPTA_CA_SUPPORTED_TOKEN_PROVIDERS_DEFAULT_VALUE;
		
		return Arrays.asList(supportedTokenProviders.trim().split("[ \t\n]*,[ \t\n]*"));
	}
	private List<String> createSupportedTokenProducts(ServletContext context){
		String supportedTokensProducts = context.getInitParameter(ACEPTA_CA_SUPPORTED_TOKEN_PRODUCTS_PROPERTY_NAME);
		
		if (supportedTokensProducts==null)
			supportedTokensProducts = ACEPTA_CA_SUPPORTED_TOKENS_PRODUCTS_DEFAULT_VALUE;
		return Arrays.asList(supportedTokensProducts.trim().split("[ \t\n]*,[ \t\n]*"));
	}
}
