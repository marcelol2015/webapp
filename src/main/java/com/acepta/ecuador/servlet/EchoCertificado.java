package com.acepta.ecuador.servlet;

import com.acepta.ecuador.security.util.BASE64;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Modulo:
 * 	Sitio Web.
 * 
 * Descripcion:
 *  Decodifica Certificado X.509 o PKCS #7 en formato PEM.
 * 
 * Dependencias:
 *  ConsultaCertificado
 *	GenCAFCert
 *  
 */
public class EchoCertificado extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String X509_CERTIFICATE_MIME_TYPE = "application/x-x509-user-cert";
	private static final String PKCS7_CERTIFICATE_MIME_TYPE = "application/x-pkcs7-certificates";
	private static final String PKCS12_CERTIFICATE_MIME_TYPE = "application/x-pkcs12";
	private static final String PEM_CERTIFICATE_MIME_TYPE = "application/x-octet-stream";
	
	private static final String DEFAULT_CERTIFICATE_MIME_TYPE = "application/x-octet-stream";
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String certificateFileName = request.getParameter("filename");
		String certificateFormat = request.getParameter("format");
		String certificateBase64 = request.getParameter("cert");

		try {
			sendCertificado(response, certificateFileName, certificateFormat, certificateBase64);
	    
			log("[Acepta] Certificado descargado (Nombre: " + certificateFileName + " Formato: " + certificateFormat + ")");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al descargar Certificado", exception);
		}
	}

	private void sendCertificado(HttpServletResponse response, String certificateFileName, String certificateFormat, String certificateBase64) throws IOException {
		response.setContentType(getContentTypeForFormat(certificateFormat));
		
	    response.setHeader("Content-Disposition", "filename=" + certificateFileName);

	    ServletOutputStream output = response.getOutputStream();
	    try {
	    	output.write(BASE64.decode(certificateBase64));
	    }
	    finally {
	    	output.close();
	    }
	}
	
	private static String getContentTypeForFormat(String format) {
		if (format.equalsIgnoreCase("X509"))
			return X509_CERTIFICATE_MIME_TYPE;
		
		if (format.equalsIgnoreCase("PKCS7"))
			return PKCS7_CERTIFICATE_MIME_TYPE;
		
		if (format.equalsIgnoreCase("PKCS12"))
			return PKCS12_CERTIFICATE_MIME_TYPE;
		
		if (format.equalsIgnoreCase("PEM"))
			return PEM_CERTIFICATE_MIME_TYPE;
		
		return DEFAULT_CERTIFICATE_MIME_TYPE;
	}
}
