package com.acepta.ecuador.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Modulo:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 *  Sin uso.
 *
 */
public class GetDescuento extends AceptaServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String codigo = request.getParameter("CodigoSolicitud");

		int descuento = getDescuentoForSolicitudWithCodigo(codigo);

		sendDescuento(response, descuento);
		
		log("[Acepta] Solicitud No. " + codigo + " tiene un descuento de $" + descuento);
	}

	private void sendDescuento(HttpServletResponse response, int descuento) throws IOException {
		response.setContentType("text/plain");
		
		PrintWriter writer = response.getWriter();
		try {
			writer.println(descuento);
		}
		finally {
			writer.close();
		}
	}
	
	private int getDescuentoForSolicitudWithCodigo(String codigo) {
		return 0;
	}
}
