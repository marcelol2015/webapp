package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.Certificado;
import com.acepta.ecuador.persistence.SolicitudPersona;
import com.acepta.ecuador.xml.XMLSolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Usuario:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Cambia el Estado de una Solicitud/Certificado.
 *
 */
public class CambiaEstadoCertificado extends AceptaServlet {
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			CambiaEstadoForm form = new CambiaEstadoForm(request);
			
			SolicitudPersona solicitud = findSolicitudForPeticion(form.getSolicitudPersona());
			
			changeEstado(solicitud, form);

			sendText(response, "SOLICITUD No. " + solicitud.getCodigo() + " " + formatearEstado(form.getEstado()));
			
			log("[Acepta] Estado de la Solicitud No. " + solicitud.getCodigo() + " cambiado a " + formatearEstado(form.getEstado()) + " (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronica") + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al cambiar el estado de la Solicitud", exception);
		}
	}

	private SolicitudPersona findSolicitudForPeticion(XMLSolicitudPersona peticion) throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			SolicitudPersona solicitud = personaBean.buscarSolicitudPersonaConNumero(peticion.getNumeroSolicitud());
			
			if (!solicitud.getClave().equals(peticion.getClaveActivacion()))
				throw new ServletException("Clave incorrecta");
			
			return solicitud;
		}
		finally {
			personaBean.close();
		}
	}
	
	private void changeEstado(SolicitudPersona solicitud, CambiaEstadoForm form) throws ServletException {
		// TODO: registrar XML al cambiar el estado de un certificado
		
		switch (form.getEstado()) {
		case SOLICITUD_ESTADO_VALIDADO:
		case SOLICITUD_ESTADO_RECHAZADO:
		case SOLICITUD_ESTADO_ANULADO:			
			changeEstadoSolicitud(solicitud, form);
			break;
			
		case SOLICITUD_ESTADO_EMITIDO:
		case SOLICITUD_ESTADO_REVOCADO:
		case SOLICITUD_ESTADO_SUSPENDIDO:
			changeEstadoCertificado(solicitud, form);
			break;
			
		default:
			throw new ServletException("Estado desconocido: " + form.getEstado());
		}
	}

	private void changeEstadoSolicitud(SolicitudPersona solicitud, CambiaEstadoForm form) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			if (form.getEstado() == SOLICITUD_ESTADO_VALIDADO)
				personaBean.aprobarSolicitudPersona(solicitud.getCodigo(), form.getData(), form.getComentario());
			
			if (form.getEstado() == SOLICITUD_ESTADO_RECHAZADO)
				personaBean.rechazarSolicitudPersona(solicitud.getCodigo(), form.getData(), form.getComentario());
			
			if (form.getEstado() == SOLICITUD_ESTADO_ANULADO)
				personaBean.anularSolicitudPersona(solicitud.getCodigo(), form.getData(), form.getComentario());
			
		}
		finally {
			personaBean.close();
		}
	}

	private void changeEstadoCertificado(SolicitudPersona solicitud, CambiaEstadoForm form) throws ServletException {
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		try {
			Certificado certificado = solicitud.getCertificado();
			
			if (certificado == null)
				throw new ServletException("Solicitud sin Certificado");
			
			if (form.getEstado() == SOLICITUD_ESTADO_EMITIDO)
				certificadoBean.restablecerCertificadoConEmisorNumeroSerie(certificado.getEmisor().getNombre(), certificado.getNumeroSerie(), form.getComentario());
			
			if (form.getEstado() == SOLICITUD_ESTADO_REVOCADO)
				certificadoBean.revocarCertificadoConEmisorNumeroSerie(certificado.getEmisor().getNombre(), certificado.getNumeroSerie(), form.getComentario());
			
			if (form.getEstado() == SOLICITUD_ESTADO_SUSPENDIDO)
				certificadoBean.suspenderCertificadoConEmisorNumeroSerie(certificado.getEmisor().getNombre(), certificado.getNumeroSerie(), form.getComentario());
		}
		finally {
			certificadoBean.close();
		}
	}
}
