package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SelloTiempoBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.concurrent.Semaphore;

/**
 * Modulo:
 * Autoridad de Sellado de Tiempo
 * 
 * Descripcion:
 * Procesa las peticiones de Sellos de Tiempo.
 */
public class SelloTiempo extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String TIMESTAMP_QUERY_MIME_TYPE = "application/timestamp-query";
	private static final String TIMESTAMP_REPLY_MIME_TYPE = "application/timestamp-reply";
	private final static Semaphore semaphore = new Semaphore(1, true);

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			byte[] timeStampRequest = receiveTimeStampRequest(request);

			if(checkNtpServerSync()){
				if (timeStampRequest != null) {
					byte[] timeStampResponse = generateTimeStampResponse(timeStampRequest);

					sendTimeStampResponse(response, timeStampResponse);

					log("[Acepta] Sello de tiempo enviado (" + timeStampResponse.length + " bytes)");
				}
				else {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST);

					log("[Acepta] Peticion de sellado de tiempo es invalida");
				}
			}else{
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

				log("[Acepta] El servidor no esta sincronizado con el servidor NTP, No se puede enviar respuesta a la peticion de tiempo.");
			}
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());

			log("[Acepta] Falla al generar Sello de tiempo", exception);
		}
	}

	private byte[] generateTimeStampResponse(byte[] timeStampRequest) throws InterruptedException {
		try{
			semaphore.acquire();
			SelloTiempoBean selloTiempoBean = getBeanFactory().createSelloTiempoBean();
			try {
				return selloTiempoBean.emitirSelloTiempo(timeStampRequest);
			}
			finally {
				selloTiempoBean.close();
			}
		}finally{
			semaphore.release();
		}
	}

	private void sendTimeStampResponse(HttpServletResponse response, byte[] timeStampResponse) throws IOException {
		response.setContentType(TIMESTAMP_REPLY_MIME_TYPE);
		response.setContentLength(timeStampResponse.length);
		
		OutputStream output = response.getOutputStream();
		try {
			output.write(timeStampResponse);
		}
		finally {
			output.close();
		}
	}

	private byte[] receiveTimeStampRequest(HttpServletRequest request) throws IOException {
		if (!request.getContentType().equals(TIMESTAMP_QUERY_MIME_TYPE))
			return null;
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = request.getInputStream();
			try {
				byte[] buffer = new byte[1024];
				int length;
				
				while ((length = input.read(buffer)) > 0)
					output.write(buffer, 0, length);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}

    private boolean checkNtpServerSync() throws InterruptedException {

		String s;
		Process p;
		float offset;
		String comando = "ntpdc -c loopinfo | awk 'NR==1{print $2; exit}'";
		String[] cmd = {"/bin/sh",
				"-c",
				comando};

		try {
			p = Runtime.getRuntime().exec(cmd);
			BufferedReader br = new BufferedReader(
					new InputStreamReader(p.getInputStream()));
			while ((s = br.readLine()) != null){
				System.out.println("[Acepta] Diferencia de tiempo en segundos: " + s);
				offset= Float.parseFloat(s);
				p.waitFor();
				p.exitValue();
				p.destroy();

				if(offset>=1 || offset<=-1){
					log("[Acepta] La diferencia de tiempo es mas de 1 seg, se detiene la generacion de TimeStamp ");
					return false;
				}else{
					log("[Acepta] La diferencia de tiempo es menos de 1 seg, El servidor esta sincronizado.");
					return  true;
				}
			}
		} catch (IOException e) {
			log("No se pudo ejecutar el comando ["+comando+"] o el valor entregado por este no se pudo leer"+ e.getMessage());
			return false;
		}
		return false;
    }
}
