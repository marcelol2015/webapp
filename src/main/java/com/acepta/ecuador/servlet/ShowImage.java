package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanException;
import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.SolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Modulo:
 * 	Sitio Web.
 * 
 * Descripcion:
 * 	Devuelve la Fotografia del Titular de una Solicitud.
 * 
 * Dependencias:
 * 	InstalacionCertificado
 *
 */
public class ShowImage extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codigo = request.getParameter("CodigoSolicitud");

		try {
			SolicitudPersona solicitud = findSolicitudWithCodigo(codigo);
			
			sendFotografiaSolicitud(response, findFotografiaWithSolicitud(solicitud));
			
			log("[Acepta] Fotografia de la Solicitud No. " + codigo + " enviada (Titular: " + solicitud.getTitular().getNombreComun() + ")");
		}
		catch (AceptaBeanException exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al devolver la fotografia de la Solicitud No. " + codigo, exception);
		}		
	}

	private SolicitudPersona findSolicitudWithCodigo(String codigo) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.buscarSolicitudPersonaConCodigo(codigo);
		}
		finally {
			personaBean.close();
		}
	}
	
	private byte[] findFotografiaWithSolicitud(SolicitudPersona solicitud) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.buscarFotografiaDeSolicitudPersona(solicitud);
		}
		finally {
			personaBean.close();
		}
	}

	private void sendFotografiaSolicitud(HttpServletResponse response, byte[] fotografia) throws IOException {
		response.setContentType(SOLICITUD_FOTOGRAFIA_MIME_TYPE);
		response.setContentLength(fotografia.length);
		
		OutputStream output = response.getOutputStream();
		try {
			output.write(fotografia);
		}
		finally {
			output.close();
		}
	}
}
