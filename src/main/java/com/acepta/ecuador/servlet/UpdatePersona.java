package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.Persona;
import com.acepta.ecuador.persistence.SolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Modulo:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 *  Corrige datos del Titular de una Solicitud.
 *
 */
public class UpdatePersona extends AceptaServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dni = decryptData(request.getParameter("DNI"));
		String nombres = decryptData(request.getParameter("Nombres"));
		String apellidoPaterno = decryptData(request.getParameter("ApPaterno"));
		String apellidoMaterno = decryptData(request.getParameter("ApMaterno"));
		String profesion = decryptData(request.getParameter("Profesion"));
		String correo = decryptData(request.getParameter("Email"));

		try {
			updateSolicitudes(dni, nombres, apellidoPaterno, apellidoMaterno, profesion, correo);
			
			sendText(response, "SOLICITUDES PENDIENTES DEL TITULAR DNI " + dni + " ACTUALIZADAS");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al actualizar las Solicitudes del Titular con DNI " + dni, exception);
		}
	}

	private void updateSolicitudes(String dni, String nombres, String apellidoPaterno, String apellidoMaterno, String profesion, String correo) throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			Persona titular = new Persona();
			
			titular.setDni(dni);
			titular.setNombres(nombres);
			titular.setApellidoPaterno(apellidoPaterno);
			titular.setApellidoMaterno(apellidoMaterno);
			titular.setProfesion(profesion);
			titular.setCorreo(correo);

			int total = 0, updated = 0;
			
			for (SolicitudPersona solicitud : personaBean.buscarSolicitudesPersonaConDniTitular(dni)) {
				if (solicitud.isPendiente() || solicitud.isAprobada() || solicitud.isRechazada()) {
					personaBean.corregirSolicitudPersona(solicitud.getCodigo(), titular);
					
					log("[Acepta] Actualizada Solicitud No. " + solicitud.getCodigo() + " del Titular con DNI " + dni);
					
					updated++;
				}
				
				total++;
			}
			
			if (updated <= 0)
				throw new ServletException("No existen Solicitudes del Titular con DNI " + dni);
			
			log("[Acepta] Se actualizaron " + updated + " de " + total + " Solicitudes del titular con DNI " + dni);
		}
		finally {
			personaBean.close();
		}
	}
}
