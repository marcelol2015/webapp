package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Modulo:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Paga y corrige la Vigencia y Autoridad una Solicitud.
 * 
 */
public class SetPago extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String codigo = request.getParameter("CodigoSolicitud");
			int mesesVigencia = Integer.parseInt(request.getParameter("Duracion"));
			long montoPagado = Long.parseLong(request.getParameter("MontoPagado"));
			boolean pagado = request.getParameter("Pagado").equalsIgnoreCase("true");
			boolean isDigital = request.getParameter("DIGITAL").equalsIgnoreCase("true");
		
			pagarSolicitud(codigo, pagado, montoPagado, isDigital, mesesVigencia);

			log("[Acepta] Pago de la Solicitud No. " + codigo + " actualizado (Clase: " + (isDigital ? "Digital" : "Electronico") + " Vigencia: " + mesesVigencia + " meses Pagada: " + (pagado ? "Si" : "No") + " Monto: $" + montoPagado + ")");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al pagar la Solicitud", exception);
		}
	}
	
	private void pagarSolicitud(String codigo, boolean pagado, long montoPagado, boolean isDigital, int mesesVigencia) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			personaBean.pagarSolicitudPersona(codigo, pagado, montoPagado, isDigital, mesesVigencia);
		}
		finally {
			personaBean.close();
		}
	}
}
