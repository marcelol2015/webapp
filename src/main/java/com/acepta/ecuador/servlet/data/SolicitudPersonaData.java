package com.acepta.ecuador.servlet.data;

import com.acepta.ecuador.persistence.Certificado;
import com.acepta.ecuador.persistence.Persona;
import com.acepta.ecuador.persistence.SolicitudPersona;

import java.util.Date;

public interface SolicitudPersonaData {
	
	Persona getTitular();
	void setTitular(Persona titular);
	
	Certificado getCertificado();
	void setCertificado(Certificado certificado);
	
	Date getFecha();

	SolicitudPersona getSolicitudPersona();

	void setSolicitudPersona(SolicitudPersona solicitudPersona);

}
