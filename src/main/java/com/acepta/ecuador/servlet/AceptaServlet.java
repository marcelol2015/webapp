package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanFactory;
import com.acepta.ecuador.security.util.BASE64;
import com.acepta.ecuador.security.util.RC4;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.StringTokenizer;


public class AceptaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/*
	 * Estados de una Solicitud/Certificado
	 */
	protected static final int SOLICITUD_ESTADO_REGISTRADO	= 0;
	protected static final int SOLICITUD_ESTADO_EMITIDO		= 1;
	protected static final int SOLICITUD_ESTADO_SUSPENDIDO	= 2;
	protected static final int SOLICITUD_ESTADO_REVOCADO	= 3;
	protected static final int SOLICITUD_ESTADO_VALIDADO	= 4;
	protected static final int SOLICITUD_ESTADO_RECHAZADO	= 5;
	protected static final int SOLICITUD_ESTADO_ANULADO	= 6;	
	protected static final int SOLICITUD_ESTADO_DESCONOCIDO	= -1;

	/*
	 * Formato de las Solicitudes
	 */	
	protected static final String SOLICITUD_XML_MIME_TYPE = "text/xml";

	/*
	 * Formato de las Fotografias en las Solicitudes
	 */
	protected static final String SOLICITUD_FOTOGRAFIA_MIME_TYPE = "image/jpeg";
	
	/*
	 * Clave Secreta de la Aplicacion del Operador de Validacion
	 */
	private static final byte[] APLICACION_CLIENTE_SECRET_KEY = "totoro123NaUsiCaA".getBytes();
	private static final String APLICACION_CLIENTE_CHARSET = "ISO-8859-1";

	private AceptaBeanFactory beanFactory;
	private VelocityEngine velocityEngine;
	private List<String> supportedTokenProviders;
	private List<String> supportedTokenProducts;	
	
	public AceptaServlet() {
	}
	
	@SuppressWarnings("unchecked")
	public void init() throws ServletException {
		setBeanFactory((AceptaBeanFactory) getServletContext().getAttribute(AceptaServletContextListener.ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME));
		setVelocityEngine((VelocityEngine) getServletContext().getAttribute(AceptaServletContextListener.VELOCITY_ENGINE_ATTRIBUTE_NAME));
		setSupportedTokenProviders((List<String>) getServletContext().getAttribute(AceptaServletContextListener.SUPPORTED_TOKEN_PROVIDERS_ATTRIBUTE_NAME));
		setSupportedTokenProducts((List<String>) getServletContext().getAttribute(AceptaServletContextListener.SUPPORTED_TOKEN_PRODUCTS_ATTRIBUTE_NAME));
	}
	
	protected AceptaBeanFactory getBeanFactory() {
		return beanFactory;
	}
	
	protected void setBeanFactory(AceptaBeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
	
	protected VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}
	
	protected void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	protected List<String> getSupportedTokenProviders() {
		return supportedTokenProviders;
	}
	protected List<String> getSupportedTokenProducts() {
		return supportedTokenProducts;
	}

	protected void setSupportedTokenProviders(List<String> supportedTokenProviders) {
		this.supportedTokenProviders = supportedTokenProviders;
	}

	protected void setSupportedTokenProducts(List<String> supportedTokenProducts) {
		this.supportedTokenProducts = supportedTokenProducts;
	}

	protected Template getVelocityTemplate(String name) throws ServletException {
		try {
			return getVelocityEngine().getTemplate(name, "UTF-8");
		}
		catch (Exception exception) {
			throw new ServletException(exception);
		}
	}
	
	protected void mergeVelocityTemplate(String contentType, String name, VelocityContext context, HttpServletResponse response) throws ServletException, ResourceNotFoundException, ParseErrorException, MethodInvocationException, IOException {
		System.out.println("template: --------------------------------> " + name);
		Template template = getVelocityTemplate(name);

		response.setContentType(contentType);
		response.setCharacterEncoding("UTF-8");
		
		PrintWriter writer = response.getWriter();
		try {
			template.merge(context, writer);
		}
		finally {
			writer.close();
		}
	}
	
	protected void sendText(HttpServletResponse response, String text) throws IOException {
		sendText(response, HttpServletResponse.SC_OK, text);
	}

	protected void sendText(HttpServletResponse response, int status, String text) throws IOException {
		response.setStatus(status);
		response.setContentType("text/plain");
		
		PrintWriter writer = response.getWriter();
		try {
			writer.println(text);
		}
		finally {
			writer.close();
		}
	}
	
	protected static boolean isInternetExplorer(HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		
		return userAgent != null && userAgent.contains("MSIE");
	}

	protected static boolean isAceptaClient(HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		
		return userAgent != null && userAgent.contains("ACEPTACLIENT");
	}

	protected static boolean isAceptaWizard(HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		
		return userAgent != null && userAgent.contains("ACEPTAWIZARD");
	}
	
	protected boolean isPosWindowsVistaSO(HttpServletRequest request){

		String userAgent = request.getHeader("User-Agent");
		StringTokenizer st = new StringTokenizer(userAgent,";");
		if (st.countTokens()>=3){
			st.nextToken();
			st.nextToken();
			String osVersion = st.nextToken();
			if (osVersion.contains("Windows NT")) {
				osVersion = osVersion.replaceFirst("Windows NT", "").trim();
				return (Float.parseFloat(osVersion) >=6.0);
			}
		}

		return false;
	}
	
	protected static String formatearEstado(int estado) {
		switch (estado) {
		case SOLICITUD_ESTADO_REGISTRADO:
			return "REGISTRADO";
		case SOLICITUD_ESTADO_EMITIDO:
			return "EMITIDO";
		case SOLICITUD_ESTADO_SUSPENDIDO:
			return "SUSPENDIDO";
		case SOLICITUD_ESTADO_REVOCADO:
			return "REVOCADO";
		case SOLICITUD_ESTADO_VALIDADO:
			return "VALIDADO";
		case SOLICITUD_ESTADO_RECHAZADO:
			return "RECHAZADO";
		case SOLICITUD_ESTADO_ANULADO:
			return "ANULADO";			
		default:
			return "DESCONOCIDO";
		}
	}
	
	public static String decryptData(String value) throws ServletException {
		try {
			if (value == null || value.length() == 0)
				return "";

			return new String(RC4.decrypt(APLICACION_CLIENTE_SECRET_KEY, BASE64.decode(value)), APLICACION_CLIENTE_CHARSET);
		}
		catch (Exception exception) {
			throw new ServletException("Falla al descifrar parametro", exception); 
		}
	}
	
	public static String encryptData(String value) throws ServletException {
		try {
			if (value == null || value.length() == 0)
				return "";

			return BASE64.encode(RC4.encrypt(APLICACION_CLIENTE_SECRET_KEY, value.getBytes(APLICACION_CLIENTE_CHARSET)));
		}
		catch (Exception exception) {
			throw new ServletException("Falla al encriptar parametro", exception); 
		}
	}
	
	public static X509Certificate getX509CertificateFromRequest(HttpServletRequest request) {
		return (X509Certificate) request.getAttribute("javax.servlet.request.X509Certificate");
	}
}
