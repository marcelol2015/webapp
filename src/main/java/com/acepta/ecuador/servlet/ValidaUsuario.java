package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AdminBean;
import com.acepta.ecuador.persistence.Grupo;
import com.acepta.ecuador.persistence.Usuario;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * @author Angelo Estartus on 20-07-16.
 */
public class ValidaUsuario extends AceptaServlet {

    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dniOperador = request.getParameter("Dni");
        String grupoOperador = request.getParameter("Grupo");

        validaUsuarioGrupo(response,dniOperador,grupoOperador);
    }

    private void validaUsuarioGrupo(HttpServletResponse response,String dniOperador, String grupoOperador) throws IOException {
        Collection<Usuario> usuarios = findUsuarios();

        boolean isValido = false;
        try {
            if(usuarios!=null){
                for (Usuario user: usuarios){
                    if(user.getDni().equalsIgnoreCase(dniOperador)){
                        for(Grupo grupo:user.getGrupos()){
                            if(grupo.getNombre().equalsIgnoreCase(grupoOperador)){
                                isValido = true;
                            }
                        }
                    }
                }
            }

            String respuesta = (isValido)? "VALIDO":"INVALIDO";
            sendText(response, respuesta);
            log("[Acepta] Log-in usuario DNI "+dniOperador+" perteneciente al grupo "+grupoOperador+" es "+respuesta);

        } catch (IOException e) {
            sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());

            log("[Acepta] Falla al validar Usuario", e);
        }
    }

    private Collection<Usuario> findUsuarios(){
        AdminBean adminBean = getBeanFactory().createAdminBean();
        try{
            return adminBean.findUsuarios();
        }finally {
            adminBean.close();
        }
    }
}
