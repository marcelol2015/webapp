package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.SolicitudPersona;
import com.acepta.ecuador.servlet.data.CertificadoPersona;
import com.acepta.ecuador.servlet.data.CertificadoPersonaACPEV1Adapter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;


public class ObtieneCertificados extends AceptaServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dni = request.getParameter("Dni");
		String estado = request.getParameter("Estado");

		obtieneCertificados(request, response, dni, estado);
	}
	
	private void obtieneCertificados(HttpServletRequest request, HttpServletResponse response, String dniTitular, String estado) throws IOException{
		try{
			sendCertificados(request, response, dniTitular, estado);
		}catch(Exception exception){
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al buscar Certificados", exception);			
		}
		
	}
	private void sendCertificados(HttpServletRequest request, HttpServletResponse response, String rutTitular, String estado) throws IOException{
		Collection<CertificadoPersona> certificados = getCertificadosPersona(rutTitular, estado);
		sendCertificadosPersona(request, response, certificados);
	}
	
	private Collection<CertificadoPersona> getCertificadosPersona(String dniTitular, String estado){
		Collection<CertificadoPersona> certificados = new ArrayList<CertificadoPersona>();
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		String[] estados = null;
		if (estado!=null)
			estados=estado.split(",");
		else{
			estados= new String[1];
			estados[0]=null;
		}
		try {
			Collection<SolicitudPersona> solicitudes = personaBean.buscarSolicitudesPersonaConDniTitular(dniTitular);
			for(SolicitudPersona solicitud: solicitudes){
				if (solicitud.isCerrada()){
					CertificadoPersona certificado = new CertificadoPersonaACPEV1Adapter(solicitud);
					for (String estadoCertificado : estados){
						if (estadoCertificado==null ||  certificado.getEstado().equals(estadoCertificado.toUpperCase()))
							certificados.add(certificado);						
					}
				}
			}

		}
		finally {
			personaBean.close();
		}
		return certificados;
	}
	
	private void sendCertificadosPersona(HttpServletRequest request, HttpServletResponse response, Collection<CertificadoPersona> certificados) throws IOException
	{
		response.setContentType("text/xml");
		
		PrintWriter writer = response.getWriter();
		try {
			writer.println("<?xml version=\"1.0\" encoding=\"" + response.getCharacterEncoding() + "\" ?>");
			writer.println("<Certificados>");
			for (CertificadoPersona certificado: certificados) {
				  sendCertificado(writer, certificado);
			}			
			writer.println("</Certificados>");
		}
		finally {
			writer.close();
		}
		
	}
	private void sendCertificado(PrintWriter writer, CertificadoPersona certificado)
	{
		writer.println("<Certificado>");		
		writer.println("<DNI>"+certificado.getDni()+"</DNI>");
		writer.println("<Nombre>"+certificado.getNombre()+"</Nombre>");		
		writer.println("<NumeroSerie>"+certificado.getNumeroSerie()+"</NumeroSerie>");
		writer.println("<Estado>"+certificado.getEstado()+"</Estado>");
		writer.println("<ValidoDesde>"+certificado.getValidoDesde()+"</ValidoDesde>");
		writer.println("<ValidoHasta>"+certificado.getValidoHasta()+"</ValidoHasta>");
		writer.println("<Clase>"+certificado.getClase()+"</Clase>");
		writer.println("<CodigoSolicitud>"+certificado.getCodigoSolicitud()+"</CodigoSolicitud>");		
		writer.println("<NumeroSolicitud>"+certificado.getNumeroSolicitud()+"</NumeroSolicitud>");
		writer.println("<Autoridad>"+certificado.getAutoridad()+"</Autoridad>");
		writer.println("<X509Cert>"+certificado.getCertificado()+"</X509Cert>");
		writer.println("</Certificado>");
	}
	
}
