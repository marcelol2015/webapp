package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.Certificado;
import com.acepta.ecuador.persistence.SolicitudPersona;
import com.acepta.ecuador.security.util.BASE64;
import com.acepta.ecuador.security.util.PIN;
import com.acepta.ecuador.security.util.PKCS7;
import com.acepta.ecuador.security.util.X509;
import org.apache.velocity.VelocityContext;

import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Modulo:
 * 	Sitio Web.
 * 
 * Descripcion:
 * 	Genera Certificado de Solicitud Persona.
 *
 * Dependencias:
 *	ShowImage
 */
public class InstalacionCertificado extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String X509_CERTIFICATE_MIME_TYPE = "application/x-x509-user-cert";
	

	private static final String VM_INSTALACION_CERTIFICADO = "/META-INF/vm/InstalacionCertificado.vm";
	private static final String VM_INSTALACION_CERTIFICADO_AUTENTICADOX509 = "/META-INF/vm/InstalacionCertificadoAutenticadoX509.vm";
	private static final String VM_INSTALACION_CERTIFICADO_ERROR = "/META-INF/vm/InstalacionCertificadoError.vm";
	private static final String VM_INSTALACION_CERTIFICADO_CONFIRMAR = "/META-INF/vm/InstalacionCertificadoConfirmar.vm";
	private static final String VM_INSTALAR_CERTIFICADO_ACEPTAR = "/META-INF/vm/InstalacionCertificadoAceptar.vm";
	
	private static final String PKCS7_CERTIFICATES_MIME_TYPE = "application/x-pkcs7-certificates";
	private static final String XML_MIME_TYPE = "text/xml";
	private static final String ERROR_RESPONSE ="ERROR";
	private static final String OK_RESPONSE ="OK";



	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (isAceptaWizard(request)) 
			response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
		else{	
			if (request.isSecure() && getX509Certificate(request)!=null ){
				seleccionaSolicitudParaInstalar(request, response);
			}
			else{
				ingresaSolicitudParaInstalar(request, response);
			}
		}
	}
	
	private void ingresaSolicitudParaInstalar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		VelocityContext context = new VelocityContext();
		
		context.put("codigoSolicitud", request.getAttribute("CodigoSolicitud"));
		
		mergeVelocityTemplate("text/html", VM_INSTALACION_CERTIFICADO, context, response);			
		
	}
	private void seleccionaSolicitudParaInstalar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		VelocityContext context = new VelocityContext();
		String dniCliente = getDniFromCertificate(request);
		boolean esAutenticadoConDigital = esAutenticadoConDigital(request);
		boolean filtrarElectronico = !esAutenticadoConDigital;
		List<Map<String,String>> listaCodSolicitudes = findCodigoSolicitudesPersonaPorDni(dniCliente, filtrarElectronico);
		if (!listaCodSolicitudes.isEmpty())
			request.getSession().setAttribute("dniCliente", dniCliente);
		if(listaCodSolicitudes.size()==1){
			confirmGeneracionCertificadoPersonaDni(request, response, listaCodSolicitudes.get(0).get("CodigoSolicitud"), dniCliente);
			log("supportedTokenProviders=" + Arrays.deepToString(getSupportedTokenProviders().toArray()));
			
		}else{
			response.addHeader("Cache-Control","no-cache");
			response.setDateHeader("Expires", 0L);
			context.put("listaCodSolicitudes", listaCodSolicitudes);
			context.put("esAutenticadoConDigital", esAutenticadoConDigital);
			mergeVelocityTemplate("text/html", VM_INSTALACION_CERTIFICADO_AUTENTICADOX509, context, response);						
			
		}
		
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String codigo = request.getParameter("CodigoSolicitud");
		String clave = request.getParameter("Pin");
		String dniCliente = (String) request.getSession().getAttribute("dniCliente");
		
		String claveEncriptada = request.getParameter("PinEncriptado");
		String pkcs10Base64 = request.getParameter("PKCS10");
		String spkacBase64 = request.getParameter("SPKAC");
		
		if (codigo != null && clave != null) {
			confirmGeneracionCertificadoPersona(request, response, codigo, clave);
		}
		else if (codigo != null && dniCliente != null && claveEncriptada==null){
			confirmGeneracionCertificadoPersonaDni(request, response, codigo, dniCliente);
		}
		else if (codigo != null && claveEncriptada != null && (pkcs10Base64 != null || spkacBase64 != null)) {
			generateCertificadoPersona(request, response, codigo, claveEncriptada, pkcs10Base64, spkacBase64);
		}
		
		log("supportedTokenProviders=" + Arrays.deepToString(getSupportedTokenProviders().toArray()));
	}
	
	private boolean esAutenticadoConDigital(HttpServletRequest request) throws ServletException{
		X509Certificate certificate = getX509Certificate(request);
		String issuerDN =certificate.getIssuerDN().getName(); 
		return issuerDN.contains("CN=Acepta Autoridad Certificadora Digital") || issuerDN.contains("CN=Acepta.pe Autoridad Certificadora Digital");
	}
	
	private X509Certificate getX509Certificate(HttpServletRequest request) throws ServletException{
		X509Certificate certificate = null;
		if (request.isSecure()){
		    X509Certificate certChain[] = (X509Certificate []) request.getAttribute ("javax.servlet.request.X509Certificate");
		    if (certChain!=null)
		      certificate = certChain[0];
		}
		return certificate;
	} 
	private String getDniFromCertificate(HttpServletRequest request) throws ServletException{
		X509Certificate certificate = getX509Certificate(request);
		String dni;
		try {
			dni = X509.decodeSubjectDNIFromCertificate(certificate);
		} catch (CertificateParsingException e) {
			throw new ServletException("No fue posible obtener Dni del certificado");
		}
		return dni;
	}
	private void confirmGeneracionCertificadoPersonaDni(HttpServletRequest request, HttpServletResponse response, String codigo, String dni) throws ServletException, IOException{
		try{
			SolicitudPersona solicitud = findSolicitudPersonaDni(codigo, dni);
			sendConfirmarGeneracionCertificadoPersona(request, response, solicitud);
			
			log("[Acepta] Confirmacion de la Solicitud No. " + codigo + " enviada (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronico") + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
		}
		catch (Exception exception) {
			sendError(request, response, exception);
			
			log("[Acepta] Falla al confirmar la instalacion de la Solicitud No. " + codigo, exception);
		}
		
	}
	private void confirmGeneracionCertificadoPersona(HttpServletRequest request, HttpServletResponse response, String codigo, String clave) throws IOException, ServletException {
		try {
			if (codigo.length() == 0)
				throw new ServletException("No se especifico el Numero de la Solicitud");
			
			if (clave.length() == 0)
				throw new ServletException("No se especifico el PIN de la Solicitud");

			String claveEncriptada = PIN.encrypt(clave);
			
			SolicitudPersona solicitud = findSolicitudPersona(codigo, claveEncriptada);
			
			sendConfirmarGeneracionCertificadoPersona(request, response, solicitud);
			
			log("[Acepta] Confirmacion de la Solicitud No. " + codigo + " enviada (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronico") + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
		}
		catch (Exception exception) {
			sendError(request, response, exception);
			
			log("[Acepta] Falla al confirmar la instalacion de la Solicitud No. " + codigo, exception);
		}
	}
	
	protected void sendConfirmarGeneracionCertificadoPersona(HttpServletRequest request, HttpServletResponse response, SolicitudPersona solicitud) throws ServletException, IOException {
		if (isAceptaWizard(request))
			sendConfirmarGeneracionCertificadoPersonaWizard(request, response, solicitud);
		else
			sendConfirmarGeneracionCertificadoPersonaBrowser(request, response, solicitud);
	}
	
	protected void sendConfirmarGeneracionCertificadoPersonaWizard(HttpServletRequest request, HttpServletResponse response, SolicitudPersona solicitud) throws ServletException, IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType(XML_MIME_TYPE);
		response.getWriter().println("<Respuesta>");
		response.getWriter().println("<Codigo>"+OK_RESPONSE+"</Codigo>");
		response.getWriter().println("<Descripcion>Solicitud Encontrada</Descripcion>");
		response.getWriter().println("<Data>");
		response.getWriter().println("<Dni>"+solicitud.getTitular().getDni()+"</Dni>");
		response.getWriter().println("<Nombre>"+solicitud.getTitular().getNombres()+" "+ solicitud.getTitular().getApellidoPaterno()+" "+solicitud.getTitular().getApellidoMaterno()+ "</Nombre>");
		response.getWriter().println("<Email>"+solicitud.getTitular().getCorreo()+"</Email>");
		response.getWriter().println("<Duracion>"+solicitud.getMesesVigencia()+"</Duracion>");
		response.getWriter().println("<TipoCertificado>"+(solicitud.getAutoridad().isDigital()?"Digital" : "Electronico")+"</TipoCertificado>");
		response.getWriter().println("<CN>"+solicitud.getTitular().getNombreComun()+"</CN>");
		if (solicitud.getAutoridad().isDigital()){
			response.getWriter().print("<ProveedoresFA>");		
			List<String> providers=getSupportedTokenProviders();
			int i=0;
			for( String provider : providers){
				if(i!=0)
					response.getWriter().print(",");
				response.getWriter().print(provider);
				i++;
			}
			response.getWriter().println("</ProveedoresFA>");
			List<String> products=getSupportedTokenProducts();
			response.getWriter().print("<TokensFA>");
			i=0;
			for(String product: products){
				if(i!=0)
					response.getWriter().print(",");
				response.getWriter().print(product);
				i++;
			}
			response.getWriter().println("</TokensFA>");
		}
		response.getWriter().println("</Data>");		
		response.getWriter().println("</Respuesta>");				
	}
	
	protected void sendConfirmarGeneracionCertificadoPersonaBrowser(HttpServletRequest request, HttpServletResponse response, SolicitudPersona solicitud) throws ServletException, IOException {
		VelocityContext context = new VelocityContext();
		
		context.put("solicitud", solicitud);
		context.put("browser", isInternetExplorer(request) ? "ie" : "mozilla");
		context.put("supportedTokenProviders", getSupportedTokenProviders());
		context.put("isWindowsPostVista", isPosWindowsVistaSO(request));
		mergeVelocityTemplate("text/html", VM_INSTALACION_CERTIFICADO_CONFIRMAR, context, response);
	}

	private SolicitudPersona findSolicitudPersona(String codigo, String clave) throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			SolicitudPersona solicitud = personaBean.buscarSolicitudPersonaConCodigo(codigo);

			if (!solicitud.getClave().equals(clave))
				throw new ServletException("PIN de la Solicitud No. " + codigo + " es incorrecto");

			if (!solicitud.isAprobada()) {
				if (solicitud.isCerrada())
					throw new ServletException("Certificado de Solicitud No. " + codigo + " ya fue generado");
				
				if (solicitud.isRechazada())
					throw new ServletException("Solicitud No. " + codigo + " fue rechazada");

				if (solicitud.isAnulada())
					throw new ServletException("Solicitud No. " + codigo + " fue anulada");				
				
				throw new ServletException("Solicitud No. " + codigo + " aun no ha sido aprobada");
			}
			
			return solicitud;
		}
		finally {
			personaBean.close();
		}
	}

	private SolicitudPersona findSolicitudPersonaDni(String codigo, String dni) throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			SolicitudPersona solicitud = personaBean.buscarSolicitudPersonaConCodigo(codigo);

			if (!solicitud.isAprobada()) {
				if (solicitud.isCerrada())
					throw new ServletException("Certificado de Solicitud No. " + codigo + " ya fue generado");
				
				if (solicitud.isRechazada())
					throw new ServletException("Solicitud No. " + codigo + " fue rechazada");
				
				if (solicitud.isAnulada())
					throw new ServletException("Solicitud No. " + codigo + " fue anulada");				
				
				throw new ServletException("Solicitud No. " + codigo + " aun no ha sido aprobada");
			}
			if (dni==null || !solicitud.getTitular().getDni().equalsIgnoreCase(dni))
				throw new ServletException("Solicitud No. " + codigo + " no correspondemal dni especificado");
			return solicitud;
		}
		finally {
			personaBean.close();
		}
	}
	
	private List<Map<String,String>> findCodigoSolicitudesPersonaPorDni(String dni, boolean filtrarElectronico) throws ServletException {
		List<Map<String, String>> listaCodigoSolicitudes = new ArrayList<Map<String,String>>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			Collection<SolicitudPersona> solicitudes = personaBean.buscarSolicitudesPersonaConDniTitular(dni);
			for(SolicitudPersona solicitud : solicitudes){
				if(solicitud.isAprobada() && (!filtrarElectronico || solicitud.getAutoridad().getNombre().equals("Electronico-V1")) ){
					Map<String, String> mapSolicitudes = new HashMap<String, String>();					
					mapSolicitudes.put("CodigoSolicitud", solicitud.getCodigo());
					mapSolicitudes.put("AutoridadIntermedia", solicitud.getAutoridad().getNombre());
					mapSolicitudes.put("FechaSolicitud",sdf.format(solicitud.getFecha()));
					listaCodigoSolicitudes.add(mapSolicitudes);					
				}
			}			
			return listaCodigoSolicitudes;
		}
		finally {
			personaBean.close();
		}
	}
	
	private void generateCertificadoPersona(HttpServletRequest request, HttpServletResponse response, String codigo, String claveEncriptada, String pkcs10Base64, String spkacBase64) throws IOException, ServletException {
		try {
			SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
			try {
				SolicitudPersona solicitud = personaBean.emitirCertificadoPersona(codigo, claveEncriptada, pkcs10Base64, spkacBase64);
			
				sendCertificadoPersona(request, response, solicitud.getCertificado().getSujeto() + ".cer", solicitud.getCertificado());
				
				log("[Acepta] Certificado de la Solicitud No. " + codigo + " generado exitosamente (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronico") + " Serie: " + solicitud.getCertificado().getNumeroSerie().toString() + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
			}
			finally {
				personaBean.close();
			}
		}
		catch (Exception exception) {
			if (exception.getCause() instanceof NoResultException)
				exception = new Exception("Solicitud inexistente en la autoridad certificadora",exception);
			
			sendError(request, response, exception);
			
			log("[Acepta] Falla al generar Certificado de la Solicitud No. " + codigo, exception);
		}
	}

	protected void sendCertificadoPersona(HttpServletRequest request, HttpServletResponse response, String filename, Certificado certificado) throws ServletException, IOException {
		if (isAceptaWizard(request))
			sendCertificadoPersonaWizard(request, response, filename, certificado);
		if (isAceptaClient(request))
			sendCertificatePersonaSistemaCliente(request,response,filename,certificado);
		else
			sendCertificadoPersonaBrowser(request, response, filename, certificado);
	}
	
	protected void sendCertificadoPersonaWizard(HttpServletRequest request, HttpServletResponse response, String filename, Certificado certificado) throws ServletException, IOException {
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		String pkcs7SignedData = BASE64.encode(PKCS7.encodePKCS7SignedData(certificadoBean.construirCadenaParaCertificado(certificado)));
		response.setContentType(PKCS7_CERTIFICATES_MIME_TYPE);
		
		response.setHeader("Content-Disposition", "filename=" + filename);
	
		OutputStream output = response.getOutputStream();
		try {
			output.write(pkcs7SignedData.getBytes());
		}
		finally {
			output.close();
		}
	}

	protected void sendCertificatePersonaSistemaCliente(HttpServletRequest request, HttpServletResponse response, String filename, Certificado certificado) throws ServletException, IOException {
	   byte[] certificateEncoded = certificado.getData();
		
		response.setContentType(X509_CERTIFICATE_MIME_TYPE);
		response.setContentLength(certificateEncoded.length);
		
		response.setHeader("Content-Disposition", "filename=" + filename);
	
		OutputStream output = response.getOutputStream();
		try {
			output.write(certificateEncoded);
		}
		finally {
			output.close();
		}				
	}

	
	protected void sendCertificadoPersonaBrowser(HttpServletRequest request, HttpServletResponse response, String filename, Certificado certificado) throws ServletException, IOException {
		if (isInternetExplorer(request)) {
			CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
			try {
				sendCertificadoToInternetExplorer(request, response, certificadoBean.construirCadenaParaCertificado(certificado));
			}
			finally {
				certificadoBean.close();
			}
		}
		else {
			sendCertificadoToMozilla(response, filename, certificado.getData());
		}
	}

	
	private void sendCertificadoToInternetExplorer(HttpServletRequest request, HttpServletResponse response, Collection<byte[]> certificateChainEncoded) throws ServletException, IOException {
		String pkcs7SignedData = BASE64.encode(PKCS7.encodePKCS7SignedData(certificateChainEncoded));
		
		VelocityContext context = new VelocityContext();
		context.put("browser", isInternetExplorer(request) ? "ie" : "mozilla");
		context.put("pkcs7Base64", pkcs7SignedData);
		context.put("isWindowsPostVista", isPosWindowsVistaSO(request));		
		mergeVelocityTemplate("text/html", VM_INSTALAR_CERTIFICADO_ACEPTAR, context, response);
	}

	private void sendCertificadoToMozilla(HttpServletResponse response, String filename, byte[] certificateEncoded) throws IOException {
		response.setContentType(X509_CERTIFICATE_MIME_TYPE);
		
		response.setHeader("Content-Disposition", "filename=" + filename);
	
		OutputStream output = response.getOutputStream();
		try {
			output.write(certificateEncoded);
		}
		finally {
			output.close();
		}
	}

	protected void sendError(HttpServletRequest request, HttpServletResponse response, Exception exception) throws ServletException, IOException {
		if  ( (isAceptaWizard(request)) || (isAceptaClient(request)) )
			sendErrorWizard(request, response, exception);
		else
			sendErrorBrowser(request, response, exception);
	}		
	 
	protected void sendErrorWizard(HttpServletRequest request, HttpServletResponse response, Exception exception) throws ServletException, IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType(XML_MIME_TYPE);
		response.getWriter().println("<Respuesta>");
		response.getWriter().println("<Codigo>"+ERROR_RESPONSE+"</Codigo>");
		response.getWriter().println("<Descripcion>"+exception.getMessage()+"</Descripcion>");
		response.getWriter().println("</Respuesta>");				
		
	}		
	
	protected void sendErrorBrowser(HttpServletRequest request, HttpServletResponse response, Exception exception) throws ServletException, IOException {
		VelocityContext context = new VelocityContext();
		
		context.put("error", exception.getMessage());
		
		mergeVelocityTemplate("text/html", VM_INSTALACION_CERTIFICADO_ERROR, context, response);
	}		
}
