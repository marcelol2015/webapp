package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.SolicitudPersona;
import com.acepta.ecuador.security.util.BASE64;
import com.acepta.ecuador.servlet.data.SolicitudPersonaAdapter;
import com.acepta.ecuador.servlet.data.SolicitudPersonaComparator;
import com.acepta.ecuador.servlet.data.SolicitudPersonaData;
import org.apache.velocity.VelocityContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.TreeSet;

/**
 * Modulo:
 *  Sitio Web.
 * 
 * Descripcion:
 * 	Busca Certificados de Persona Natural por DNI o correo del Titular.
 *  Busca Certificados de Sitio Web por nombre de Dominio.
 * 
 * Dependencias:
 * 	EchoCertificado
 * 
 */
public class ConsultaCertificado extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String VM_CONSULTA_CERTIFICADO = "/META-INF/vm/ConsultaCertificado.vm";
	private static final String VM_CONSULTA_CERTIFICADO_ERROR = "/META-INF/vm/ConsultaCertificadoError.vm";
	private static final String VM_CONSULTA_CERTIFICADO_PERSONA_RESULTADO = "/META-INF/vm/ConsultaCertificadoResultadoPersona.vm";


	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		mergeVelocityTemplate("text/html", VM_CONSULTA_CERTIFICADO, new VelocityContext(), response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dniTitular = request.getParameter("DNI");
		String correoTitular = request.getParameter("Correo");

		try {
			if (dniTitular != null) {
				findSolicitudesWithDniTitular(request, response, normalizeDniTitular(dniTitular));
			}
			else if (correoTitular != null) {
				findSolicitudesWithCorreoTitular(request, response, normalizeCorreoTitular(correoTitular));
			}
			else {
				throw new ServletException("No se especifico ningun parametro de busqueda");
			}
			
			log("[Acepta] Consulta de Certificados exitosa (" + (dniTitular != null ? "DNI: " + dniTitular : correoTitular != null ? "Correo: " + correoTitular : ")"));
		}
		catch (Exception exception) {
			sendError(request, response, exception);

			log("[Acepta] Falla al consultar Certificados", exception);
		}
	}

	private void findSolicitudesWithDniTitular(HttpServletRequest request, HttpServletResponse response, String dniTitular) throws IOException, ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			Collection<SolicitudPersona> solicitudes = personaBean.buscarSolicitudesPersonaConDniTitular(dniTitular);
			TreeSet<SolicitudPersonaData> solicitudesRespuesta = new TreeSet<SolicitudPersonaData>(new SolicitudPersonaComparator());
			for( SolicitudPersona solicitud:solicitudes){
				solicitudesRespuesta.add(new SolicitudPersonaAdapter(solicitud));
			}

            sendSolicitudesPersona(request,response,solicitudesRespuesta);
		}
		finally {
			personaBean.close();
		}
	}

	private void findSolicitudesWithCorreoTitular(HttpServletRequest request, HttpServletResponse response, String correoTitular) throws IOException, ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			TreeSet<SolicitudPersonaData> solicitudesRespuesta = new TreeSet<SolicitudPersonaData>(new SolicitudPersonaComparator());			
			Collection<SolicitudPersona> solicitudes = personaBean.buscarSolicitudesPersonaConCorreoTitular(correoTitular);
			for(SolicitudPersona solicitud: solicitudes){
				solicitudesRespuesta.add(new SolicitudPersonaAdapter(solicitud));
			}
            sendSolicitudesPersona(request,response,solicitudesRespuesta);
		}
		finally {
			personaBean.close();
		}
	}

	private void sendSolicitudesPersona(HttpServletRequest request, HttpServletResponse response, Collection<SolicitudPersonaData> solicitudes) throws ServletException, IOException {
		VelocityContext context = new VelocityContext();
		
		context.put("solicitudes", solicitudes);
		context.put("fecha", new SimpleDateFormat("yyyy-MM-dd"));
		context.put("fechahora", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));		
		context.put("base64", new BASE64());

		mergeVelocityTemplate("text/html", VM_CONSULTA_CERTIFICADO_PERSONA_RESULTADO, context, response);
	}

	private void sendError(HttpServletRequest request, HttpServletResponse response, Exception exception) throws ServletException, IOException {
		VelocityContext context = new VelocityContext();
		
		context.put("error", exception.getMessage());
		
		mergeVelocityTemplate("text/html", VM_CONSULTA_CERTIFICADO_ERROR, context, response);
	}

	private static String normalizeCorreoTitular(String correoTitular) {
		if (correoTitular != null)
			correoTitular = correoTitular.trim();
		return correoTitular;
	}

	private static String normalizeDniTitular(String dniTitular) {
		if (dniTitular != null)
			dniTitular = dniTitular.trim().replace(".", "");
		return dniTitular;
	}

}
