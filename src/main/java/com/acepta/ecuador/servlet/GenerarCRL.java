package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanFactory;
import com.acepta.ecuador.bean.CertificadoBean;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.security.cert.CRLException;
import java.util.TimerTask;


/**
 * Modulo:
 * 	Sistema
 * 
 * Descripcion:
 * 	Tarea periodica que genera las CRLs de las Autoridades.
 */
public class GenerarCRL extends TimerTask {
	public static final int CRL_DURATION = 24;
	public static final int CRL_ROOT_DURATION = 4380;
	
	private ServletContext servletContext;
	
	public GenerarCRL() {
	}
	
	public long getTimerPeriod() {
		return CRL_DURATION * 3600L * 1000L;
	}
	
	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public AceptaBeanFactory getBeanFactory() {
		return (AceptaBeanFactory) getServletContext().getAttribute(AceptaServletContextListener.ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME);
	}

	public void run() {
		try {
			generateCRLs();
		}
		catch (Exception exception) {
			log("[Acepta] Falla al generar CRLs", exception);
		}
	}

	private void generateCRLs() throws IOException, CRLException {
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		try {
			for (String nombreAutoridad : certificadoBean.getNombresAutoridadesCertificadoras()) {
				if(!nombreAutoridad.equals("EcuadorRaiz-V1")){
					certificadoBean.generarCRLConEmisorVigencia(nombreAutoridad, CRL_DURATION);

					log("[Acepta] CRL generada exitosamente (Autoridad: " + nombreAutoridad + " Vigencia: " + CRL_DURATION + " horas)");
				}
			}
		}
		finally {
			certificadoBean.close();
		}
	}

	private void generateCRLRaiz() throws IOException, CRLException {
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		String nombreAutoridad = "EcuadorRaiz-V1";
		try {
				certificadoBean.generarCRLConEmisorVigencia(nombreAutoridad, CRL_ROOT_DURATION);
				log("[Acepta] CRL generada exitosamente (Autoridad: " + nombreAutoridad + " Vigencia: " + CRL_ROOT_DURATION + " horas)");
		}
		finally {
			certificadoBean.close();
		}
	}

	private void log(String message) {
		getServletContext().log(message);
	}

	private void log(String message, Exception exception) {
		getServletContext().log(message, exception);
	}
}
