package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.Certificado;
import com.acepta.ecuador.persistence.SolicitudPersona;
import com.acepta.ecuador.xml.XMLSolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Usuario:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Revoca Certificado de Firma Electronica.
 *
 */
public class RevocaCertificado extends AceptaServlet {
	private static final long serialVersionUID = 1L;
	
	protected static final int CERTIFICADO_REVOCADO_KEY_COMPROMISE = 1;
	protected static final int CERTIFICADO_REVOCADO_SUPERSEDED = 4;
	protected static final int CERTIFICADO_REVOCADO_CESSATION_OF_OPERATION = 5;
	protected static final int CERTIFICADO_REVOCADO_CERTIFICATE_HOLD = 6;
	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			CambiaEstadoForm form = new CambiaEstadoForm(request);
			
			SolicitudPersona solicitud = findSolicitudForPeticion(form.getSolicitudPersona());
			
			changeEstado(solicitud, form);

			sendText(response, "SOLICITUD No. " + solicitud.getCodigo() + " " + formatearEstado(form.getEstado()));
			
			log("[Acepta] Estado de Certificado Solicitud No. " + solicitud.getCodigo() + " cambiado a " + formatearEstado(form.getEstado()) + " (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronico") + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al revocar certificado", exception);
		}
	}

	private SolicitudPersona findSolicitudForPeticion(XMLSolicitudPersona peticion) throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			SolicitudPersona solicitud = personaBean.buscarSolicitudPersonaConNumero(peticion.getNumeroSolicitud());
			
			if (!solicitud.getClave().equals(peticion.getClaveActivacion()))
				throw new ServletException("Clave incorrecta");
			
			return solicitud;
		}
		finally {
			personaBean.close();
		}
	}
	
	private void changeEstado(SolicitudPersona solicitud, CambiaEstadoForm form) throws ServletException {
		// TODO: registrar XML al cambiar el estado de un certificado
		
		switch (form.getEstado()) {		
		case CERTIFICADO_REVOCADO_KEY_COMPROMISE:
		case CERTIFICADO_REVOCADO_SUPERSEDED:
		case CERTIFICADO_REVOCADO_CESSATION_OF_OPERATION:
		case CERTIFICADO_REVOCADO_CERTIFICATE_HOLD:
			revocarCertificado(solicitud, form);
			break;
			
		default:
			throw new ServletException("Estado desconocido: " + form.getEstado());
		}
	}

	private void revocarCertificado(SolicitudPersona solicitud, CambiaEstadoForm form) throws ServletException {
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		try {
			Certificado certificado = solicitud.getCertificado();
			Certificado.Razon razonRevocacion = Certificado.Razon.UNUSED;
			
			if (certificado == null)
				throw new ServletException("Solicitud sin Certificado");
			switch(form.getEstado()) {
			case CERTIFICADO_REVOCADO_KEY_COMPROMISE:
				razonRevocacion=Certificado.Razon.KEY_COMPROMISE;
				break;
			case CERTIFICADO_REVOCADO_SUPERSEDED:
				razonRevocacion=Certificado.Razon.SUPERSEDED;
				break;				 
			case CERTIFICADO_REVOCADO_CESSATION_OF_OPERATION:
				razonRevocacion=Certificado.Razon.CESSATION_OF_OPERATION;
				break;
			case CERTIFICADO_REVOCADO_CERTIFICATE_HOLD:
				razonRevocacion=Certificado.Razon.CERTIFICATE_HOLD;
				break;
			}
			certificadoBean.revocarCertificadoConEmisorNumeroSerie(certificado.getEmisor().getNombre(), certificado.getNumeroSerie(), razonRevocacion, form.getComentario());
		}
		finally {
			certificadoBean.close();
		}
	}
	
	protected static String formatearEstado(int estado) {
		switch (estado) {
		case CERTIFICADO_REVOCADO_KEY_COMPROMISE:
			return "REVOCADO(LLAVE COMPROMETIDA)";
		case CERTIFICADO_REVOCADO_SUPERSEDED:
			return "REVOCADO(REEMPLAZO DE CERTIFICADO)";
		case CERTIFICADO_REVOCADO_CESSATION_OF_OPERATION:
			return "REVOCADO(PETICION DE REVOCACION)";
		case CERTIFICADO_REVOCADO_CERTIFICATE_HOLD:
			return "REVOCADO(SUSPENDIDO)";
		default:
			return "DESCONOCIDO";
		}
	}

}
