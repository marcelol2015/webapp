package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.persistence.SolicitudSelloTiempo;
import com.acepta.ecuador.security.util.PKCS7;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

/**
 * Modulo:
 * 	Aplicacion Operador Registro/Validacion SelloTiempo.
 * 
 * Descripcion:
 * 	Ingresa una nueva Solicitud de SelloTiempo.
 */
public class IngresoSolicitudSelloTiempo extends AceptaServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String PKCS7_MIME_TYPE = "application/x-pkcs7-mime";

	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			SolicitudSelloTiempo solicitud = ingresarSolicitud(getSolicitudFromRequest(request));
	
			sendCertificado(response, getCertificadoFileNameForSolicitud(solicitud), getCadenaCertificadoForSolicitud(solicitud));
		
			log("[Acepta] Solicitud SelloTiempo No. " + solicitud.getCodigo() + " ingresada (Nombre: " + solicitud.getSistema().getNombre() + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());

			log("[Acepta] Falla al ingresar Solicitud", exception);
		}
	}

	private void sendCertificado(HttpServletResponse response, String filename, Collection<byte[]> certificates) throws IOException {
		byte[] pkcs7SignedData = PKCS7.encodePKCS7SignedData(certificates);

		response.setContentType(PKCS7_MIME_TYPE);
		response.setContentLength(pkcs7SignedData.length);
		
		response.setHeader("Content-Disposition", "filename=" + filename);
	
		OutputStream output = response.getOutputStream();
		try {
			output.write(pkcs7SignedData);
		}
		finally {
			output.close();
		}
	}
	
	private Collection<byte[]> getCadenaCertificadoForSolicitud(SolicitudSelloTiempo solicitud) {
		CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
		try {
			return certificadoBean.construirCadenaParaCertificado(solicitud.getCertificado());
		}
		finally {
			certificadoBean.close();
		}
	}
	
	private String getCertificadoFileNameForSolicitud(SolicitudSelloTiempo solicitud) {
		return solicitud.getSistema().getNombre() + ".p7m";
	}

	private SolicitudSelloTiempo ingresarSolicitud(byte[] data) {
		//SolicitudSelloTiempoBean selloTiempoBean = getBeanFactory().createSolicitudSelloTiempoBean();
		try {
			return null; //TODO: return selloTiempoBean.ingresarSolicitudSistema(data);
		}
		finally {
            //selloTiempoBean.close();
		}
	}
	
	private byte[] getSolicitudFromRequest(HttpServletRequest request) throws ServletException, IOException {
		if (request.getContentType() == null || !request.getContentType().equals("text/xml"))
			throw new ServletException("Solo se aceptan Solicitudes en formato XML");
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = request.getInputStream();
			try {
				byte[] buffer = new byte[1024];
				int length;
				
				while ((length = input.read(buffer)) > 0)
					output.write(buffer, 0, length);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}
}
