package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanException;
import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.security.util.BASE64;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;

/**
 * Responde peticiones OCSP de las Autoridades Certificadoras.
 * 
 * TODO: Soporte de peticiones que llegan por HTTP POST.
 * 
 * @author Carlos Hasan
 * @version 1.0, 2008-10-16
 *
 */
public class GetOCSP extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String OCSP_REQUEST_MIME_TYPE = "application/ocsp-request";
	private static final String OCSP_RESPONSE_MIME_TYPE = "application/ocsp-response";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetOCSP(request, response, true);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetOCSP(request, response, false);
	}
	
	private void doGetOCSP(HttpServletRequest request, HttpServletResponse response, boolean isGetRequest) throws ServletException, IOException {
		try {
			byte[] peticion = getPeticionOCSPFromRequest(request, isGetRequest);
			
			String nombreAutoridad = getNombreAutoridadFromRequest(request);
	
			byte[] ocspResponse = generateOCSP(nombreAutoridad, peticion);
	
			if (ocspResponse != null) {
				sendOCSP(response, ocspResponse);
				
				log("[Acepta] Respuesta OCSP enviada (Autoridad: " + nombreAutoridad + ")");
			}
			else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				
				log("[Acepta] No existe la Autoridad: " + nombreAutoridad);
			}
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al devolver OCSP", exception);
		}
	}

	private void sendOCSP(HttpServletResponse response, byte[] ocspResponse) throws IOException {
		response.setContentType(OCSP_RESPONSE_MIME_TYPE);
		response.setContentLength(ocspResponse.length);
		
		response.setDateHeader("Expires", System.currentTimeMillis());
		
		OutputStream output = response.getOutputStream();
		try {
			output.write(ocspResponse);
		}
		finally {
			output.close();
		}
	}
	
	private String getNombreAutoridadFromRequest(HttpServletRequest request) throws ServletException {
		String pathInfo = request.getPathInfo();
	
		if (pathInfo.startsWith("/")) {
			int index = pathInfo.indexOf("/", 1);
			
			String nombreAutoridad = (index >= 0 ? pathInfo.substring(1, index) : pathInfo.substring(1));
			
			if (nombreAutoridad.matches("[A-Za-z][A-Za-z0-9-]*"))
				return nombreAutoridad;
		}
		
		throw new ServletException("No es posible determinar la Autoridad de la peticion: " + pathInfo);
	}

	private byte[] getPeticionOCSPFromRequest(HttpServletRequest request, boolean isGetRequest) throws ServletException, IOException {
		if (isGetRequest)
			return getPeticionOCSPFromRequestURL(request);
		else
			return getPeticionOCSPFromRequestBody(request);
	}
	
	private byte[] getPeticionOCSPFromRequestURL(HttpServletRequest request) throws ServletException {
		String pathInfo = request.getPathInfo().replace("\n", "").replace("\r", "");
		
		if (Pattern.compile("/[A-Za-z][A-Za-z0-9-]*/.+", Pattern.DOTALL).matcher(pathInfo).matches())
			return BASE64.decode(pathInfo.substring(pathInfo.indexOf("/", 1) + 1));
		
		throw new ServletException("Peticion OCSP invalida: " + pathInfo);
	}

	private byte[] getPeticionOCSPFromRequestBody(HttpServletRequest request) throws ServletException, IOException {
		if (!request.getContentType().equals(OCSP_REQUEST_MIME_TYPE))
			throw new ServletException("Peticion OCSP invalida: " + request.getContentType());
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			InputStream input = request.getInputStream();
			try {
				byte[] buffer = new byte[1024];
				int length;
				
				while ((length = input.read(buffer)) > 0)
					output.write(buffer, 0, length);
			}
			finally {
				input.close();
			}
		}
		finally {
			output.close();
		}
		
		return output.toByteArray();
	}
	
	private byte[] generateOCSP(String nombreAutoridad, byte[] peticion) throws ServletException {
		try {
			CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
			try {
				if (certificadoBean.getNombresAutoridadesCertificadoras().contains(nombreAutoridad))
					return certificadoBean.generarOCSP(nombreAutoridad, peticion);
			}
			finally {
				certificadoBean.close();
			}
			
			return null;
		}
		catch (AceptaBeanException exception) {
			throw new ServletException("Falla al generar respuesta OCSP para la Autoridad: " + nombreAutoridad, exception);
		}
	}
}
