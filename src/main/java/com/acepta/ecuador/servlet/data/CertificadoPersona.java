package com.acepta.ecuador.servlet.data;

public interface CertificadoPersona {
	String VIGENTE = "VIGENTE";
	String EXPIRADO = "EXPIRADO";
	String SUSPENDIDO = "SUSPENDIDO";
	String REVOCADO = "REVOCADO";

	String DIGITAL = "DIGITAL";
	String ELECTRONICA = "ELECTRONICA";
	String TRIBUTARIA = "TRIBUTARIA";

	String ACPEV1 = "ACPEV1";

	String getDni();

	String getNombre();

	String getNumeroSerie();

	String getEstado();

	String getValidoDesde();

	String getValidoHasta();

	String getClase();

	String getCodigoSolicitud();

	String getNumeroSolicitud();

	String getAutoridad();

	String getCertificado();
}
