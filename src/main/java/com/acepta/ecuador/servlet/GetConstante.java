package com.acepta.ecuador.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Modulo:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Devuelve parametro de la configuracion de la aplicacion. 
 */
public class GetConstante extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("Nombre");

		String valor = findConstanteWithNombre(nombre);

		response.setContentType("text/plain");
		
		PrintWriter writer = response.getWriter();
		try {
			writer.println(valor);
		}
		finally {
			writer.close();
		}
		
		log("[Acepta] Constante " + nombre + " con valor " + valor);
	}

	private String findConstanteWithNombre(String nombre) {
		// TODO: devolver constante de la configuracion
		return "0";
	}
}
