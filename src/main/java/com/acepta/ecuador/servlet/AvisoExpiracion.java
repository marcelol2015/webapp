package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanFactory;
import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.SolicitudPersona;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

/**
 * Tarea que periodicamente enviar correos con Avisos de Expiracion de certificados.
 * 
 * @author Angelo Estartus
 * @version 1.0, 2017-06-01
 *
 */
public class AvisoExpiracion extends TimerTask {
	private static final String CORREO_AVISO_EXPIRACION_SUBJECT = "Expiracion de Certificado";
	private static final String CORREO_AVISO_EXPIRACION_FROM = "info@acepta.com";
	private static final String CORREO_AVISO_EXPIRACION_BCC = "crm.acepta@gmail.com";
	private static final String CORREO_AVISO_EXPIRACION_CONTENT_TYPE = "text/html";
	private static final String CORREO_AVISO_EXPIRACION_CONTENT_TEMPLATE = "META-INF/vm/AvisoExpiracion.vm";

	private static final int AVISO_EXPIRACION_ANTICIPACION_EN_DIAS = 7; 
	private static final int AVISO_EXPIRACION_PERIODO_EN_HORAS = 24;
	
	private ServletContext servletContext;
	
	public AvisoExpiracion() {
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public long getTimerPeriod() {
		return AVISO_EXPIRACION_PERIODO_EN_HORAS * 3600L * 1000L;
	}
	
	protected AceptaBeanFactory getBeanFactory() {
		return (AceptaBeanFactory) getServletContext().getAttribute(AceptaServletContextListener.ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME);
	}

	protected VelocityEngine getVelocityEngine() {
		return (VelocityEngine) getServletContext().getAttribute(AceptaServletContextListener.VELOCITY_ENGINE_ATTRIBUTE_NAME);
	}

	protected Session getMailSession() {
		return (Session) getServletContext().getAttribute(AceptaServletContextListener.MAIL_SESSION_ATTRIBUTE_NAME);
	}

	public void run() {
		try {
			enviarAvisosDeExpiracion();
		}
		catch (Exception exception) {
			log("[Acepta] Falla al enviar los avisos de expiracion", exception);
		}
	}

	private void enviarAvisosDeExpiracion() throws NoSuchProviderException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			Date comienzoPeriodoExpiracion = getComienzoPeriodoExpiracion();
			Date terminoPeriodoExpiracion = getTerminoPeriodoExpiracion();
			Session session = getMailSession();	
			Transport transport = session.getTransport();
			log("[Acepta] Buscando certificados que expiran entre " + comienzoPeriodoExpiracion + " y " + terminoPeriodoExpiracion);
			
			for (SolicitudPersona solicitud : personaBean.buscarSolicitudesPersonaConExpiracionEntre(comienzoPeriodoExpiracion, terminoPeriodoExpiracion)) {
				if (!solicitud.getCertificado().isRevocado())
					enviarAvisoExpiracion(solicitud, session, transport);
			}
			if (transport.isConnected())
				  transport.close();
			
		} catch (MessagingException e) {
		}
		finally {
			personaBean.close();
		}
	}
	
	private void enviarAvisoExpiracion(SolicitudPersona solicitud, Session session, Transport transport) {
		try {
			String aviso = crearAvisoExpiracionForSolicitud(solicitud);
	
			enviarAvisoExpiracion(solicitud, aviso, session, transport);
			
			log("[Acepta] Aviso de expiracion enviado al titular " + solicitud.getTitular().getNombreComun() + " (" + solicitud.getTitular().getCorreo() + ") de la Solicitud No. " + solicitud.getCodigo() + " cuyo certificado expira el " + solicitud.getCertificado().getValidoHasta());
		}
		catch (Exception exception) {
			log("[Acepta] Falla al enviar aviso de expiracion del certificado de la Solicitud No. " + solicitud.getCodigo(), exception);
		}
	}

	private String crearAvisoExpiracionForSolicitud(SolicitudPersona solicitud) throws ResourceNotFoundException, ParseErrorException, Exception {
		Template template = getVelocityEngine().getTemplate(CORREO_AVISO_EXPIRACION_CONTENT_TEMPLATE, "UTF-8");
	
		VelocityContext context = new VelocityContext();

		context.put("solicitud", solicitud);
		context.put("fecha", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		
		StringWriter writer = new StringWriter();
		try {
			template.merge(context, writer);
		}
		finally {
			writer.close();
		}
		
		return writer.toString();
	}

	private void enviarAvisoExpiracion(SolicitudPersona solicitud, String aviso, Session session, Transport transport) throws Exception, MessagingException, NoSuchProviderException {

		if (!transport.isConnected())
		  transport.connect();
		MimeMessage message = new MimeMessage(session);
		
		message.setSubject(CORREO_AVISO_EXPIRACION_SUBJECT);
		message.setFrom(new InternetAddress(CORREO_AVISO_EXPIRACION_FROM));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(solicitud.getTitular().getCorreo()));
		message.setRecipient(Message.RecipientType.BCC, new InternetAddress(CORREO_AVISO_EXPIRACION_BCC));
		
		MimeMultipart multipart = new MimeMultipart();
		MimeBodyPart bodyPart = new MimeBodyPart();
		
		bodyPart.setContent(aviso, CORREO_AVISO_EXPIRACION_CONTENT_TYPE);
		multipart.addBodyPart(bodyPart);
		
		message.setContent(multipart);
		
		message.saveChanges();
		
		transport.sendMessage(message, message.getAllRecipients());
	}

	private Date getComienzoPeriodoExpiracion() {
		Calendar calendar = Calendar.getInstance();
		
		calendar.clear(Calendar.HOUR_OF_DAY);
		calendar.clear(Calendar.HOUR);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);
		
		calendar.add(Calendar.DAY_OF_MONTH, AVISO_EXPIRACION_ANTICIPACION_EN_DIAS);
		
		return calendar.getTime();
	}
	
	private Date getTerminoPeriodoExpiracion() {
		Calendar calendar = Calendar.getInstance();

		calendar.clear(Calendar.HOUR_OF_DAY);
		calendar.clear(Calendar.HOUR);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);

		calendar.add(Calendar.DAY_OF_MONTH, AVISO_EXPIRACION_ANTICIPACION_EN_DIAS);
		calendar.add(Calendar.HOUR_OF_DAY, AVISO_EXPIRACION_PERIODO_EN_HORAS);
		calendar.add(Calendar.MILLISECOND, -1);

		return calendar.getTime();
	}
	
	private void log(String message) {
		getServletContext().log(message);
	}

	private void log(String message, Exception exception) {
		getServletContext().log(message, exception);
	}
}
