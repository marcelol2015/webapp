package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.AceptaBeanFactory;
import com.acepta.ecuador.bean.CertificadoBean;
import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.CRL;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import static com.acepta.ecuador.security.authority.util.PropertiesUtilUrl.RAIZ_CRL_NOTIFICATION_CC;
import static com.acepta.ecuador.security.authority.util.PropertiesUtilUrl.RAIZ_CRL_NOTIFICATION_TO;

/**
 * Tarea que periodicamente enviar correos con Avisos de Expiracion de certificados.
 * 
 * @author Angelo Estartus
 * @version 1.0, 2017-06-01
 *
 */

public class NotificacionExpiracionCRL extends TimerTask {
	private static final String CORREO_AVISO_EXPIRACION_SUBJECT = "Pronta Expiracion de CRL Raiz";
	private static final String CORREO_AVISO_EXPIRACION_FROM = "info@acepta.com";
	private static final String CORREO_AVISO_EXPIRACION_BCC = "marcelol@loso.cl.com";
    private static final String CORREO_AVISO_EXPIRACION_TO = RAIZ_CRL_NOTIFICATION_TO;
    private static final String CORREO_AVISO_EXPIRACION_CC = RAIZ_CRL_NOTIFICATION_CC;
	private static final String CORREO_AVISO_EXPIRACION_CONTENT_TYPE = "text/html";
	private static final String CORREO_AVISO_EXPIRACION_CONTENT_TEMPLATE = "META-INF/vm/NotificacionExpiracionCRL.vm";

	private static final int PRIMERA_NOTIFICACION_DIAS = 15;
	private static final int SEGUNDA_NOTIFICACION_DIAS = 5;

	private static final String AUTORIDAD_RAIZ = "EcuadorRaiz-V1";

	private ServletContext servletContext;

	public NotificacionExpiracionCRL() {
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	protected AceptaBeanFactory getBeanFactory() {
		return (AceptaBeanFactory) getServletContext().getAttribute(AceptaServletContextListener.ACEPTA_BEAN_FACTORY_ATTRIBUTE_NAME);
	}

	protected VelocityEngine getVelocityEngine() {
		return (VelocityEngine) getServletContext().getAttribute(AceptaServletContextListener.VELOCITY_ENGINE_ATTRIBUTE_NAME);
	}

	protected Session getMailSession() {
		return (Session) getServletContext().getAttribute(AceptaServletContextListener.MAIL_SESSION_ATTRIBUTE_NAME);
	}

	public void run() {
		try {
			enviarAvisosDeExpiracion();
		}
		catch (Exception exception) {
			log("[Acepta] Falla al enviar las notificaciones de expiracion de CRL", exception);
		}
	}

        private void enviarAvisosDeExpiracion() throws NoSuchProviderException {
            SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
            CertificadoBean certificadoBean = getBeanFactory().createCertificadoBean();
            try {
                Date cincoDiasExpiracion = getCincoDiasParaExpiracion();
                Session session = getMailSession();
                Transport transport = session.getTransport();
                CRL raizCRL = certificadoBean.buscarUltimaCRLConEmisor(AUTORIDAD_RAIZ);

                if(cincoDiasExpiracion.after(raizCRL.getValidoHasta())){
                    enviarAvisoExpiracionRaiz(raizCRL, session, transport);
                }
                if (transport.isConnected())
                      transport.close();

            } catch (MessagingException e) {
                log("[Acepta] Falla al enviar aviso de expiracion enviado al correo " + CORREO_AVISO_EXPIRACION_TO +" con copia a "+CORREO_AVISO_EXPIRACION_CC, e);
            } finally {
                personaBean.close();
            }
        }

        private void enviarAvisoExpiracionRaiz(CRL raizCRL, Session session, Transport transport) {
            try {
                String aviso = crearAvisoExpiracionForRoot(raizCRL);

                enviarAvisoExpiracion(aviso, session, transport);

                log("[Acepta] Aviso de expiracion enviado al correo " + CORREO_AVISO_EXPIRACION_TO +" con copia a "+CORREO_AVISO_EXPIRACION_CC);
            }
            catch (Exception exception) {
                log("[Acepta] Falla al enviar aviso de expiracion enviado al correo " + CORREO_AVISO_EXPIRACION_TO +" con copia a "+CORREO_AVISO_EXPIRACION_CC, exception);
            }
        }

        private String crearAvisoExpiracionForRoot(CRL raizCRL) throws ResourceNotFoundException, ParseErrorException, Exception {
            Template template = getVelocityEngine().getTemplate(CORREO_AVISO_EXPIRACION_CONTENT_TEMPLATE, "UTF-8");

            VelocityContext context = new VelocityContext();
            context.put("fecha", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            context.put("crl", raizCRL);

            StringWriter writer = new StringWriter();
            try {
                template.merge(context, writer);
            }
            finally {
                writer.close();
            }

            return writer.toString();
        }

        private void enviarAvisoExpiracion(String aviso, Session session, Transport transport) throws Exception, MessagingException, NoSuchProviderException {

            if (!transport.isConnected())
              transport.connect();
            MimeMessage message = new MimeMessage(session);

            message.setSubject(CORREO_AVISO_EXPIRACION_SUBJECT);
            message.setFrom(new InternetAddress(CORREO_AVISO_EXPIRACION_FROM));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(CORREO_AVISO_EXPIRACION_TO));
            message.setRecipient(Message.RecipientType.CC, new InternetAddress(CORREO_AVISO_EXPIRACION_CC));
            message.setRecipient(Message.RecipientType.BCC, new InternetAddress(CORREO_AVISO_EXPIRACION_BCC));

            MimeMultipart multipart = new MimeMultipart();
            MimeBodyPart bodyPart = new MimeBodyPart();

            bodyPart.setContent(aviso, CORREO_AVISO_EXPIRACION_CONTENT_TYPE);
            multipart.addBodyPart(bodyPart);

            message.setContent(multipart);

            message.saveChanges();

            transport.sendMessage(message, message.getAllRecipients());
        }

    private Date getQuinceDiasParaExpiracion() {
        Calendar calendar = Calendar.getInstance();

        calendar.clear(Calendar.HOUR_OF_DAY);
        calendar.clear(Calendar.HOUR);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);

        calendar.add(Calendar.DAY_OF_MONTH, PRIMERA_NOTIFICACION_DIAS);

        return calendar.getTime();
    }

	private Date getCincoDiasParaExpiracion() {
		Calendar calendar = Calendar.getInstance();
		
		calendar.clear(Calendar.HOUR_OF_DAY);
		calendar.clear(Calendar.HOUR);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);
		
		calendar.add(Calendar.DAY_OF_MONTH, SEGUNDA_NOTIFICACION_DIAS);
		
		return calendar.getTime();
	}
	

	
	private void log(String message) {
		getServletContext().log(message);
	}

	private void log(String message, Exception exception) {
		getServletContext().log(message, exception);
	}
}

