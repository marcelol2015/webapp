package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.Documento;
import com.acepta.ecuador.persistence.SolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;


/**
 * Modulo:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Devuelve documento XML de una Solicitud.
 * 
 */
public class GetXML extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String codigo = request.getParameter("FileName");

	    try {
	    	SolicitudPersona solicitud = findSolicitudWithCodigo(codigo);
	    
	    	Documento documento = findDocumentoWithSolicitud(solicitud);
	    	
			sendDocumentoSolicitud(response, documento);
	    	
	    	log("[Acepta] Solicitud No. " + solicitud.getCodigo() + " enviada (Titular: " + solicitud.getTitular().getNombreComun() + " Clase: " + (solicitud.getAutoridad().isDigital() ? "Digital" : "Electronica") + " Vigencia: " + solicitud.getMesesVigencia() + " meses)");
	    }
	    catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
	    	
	    	log("[Acepta] Falla al enviar la Solicitud No. " + codigo, exception);
	    }
	}

	private SolicitudPersona findSolicitudWithCodigo(String codigo) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.buscarSolicitudPersonaConCodigo(codigo);
		}
		finally {
			personaBean.close();
		}
	}
	
	private Documento findDocumentoWithSolicitud(SolicitudPersona solicitud) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.buscarDocumentoDeSolicitudPersona(solicitud);
		}
		finally {
			personaBean.close();
		}
	}
	
	private void sendDocumentoSolicitud(HttpServletResponse response, Documento documento) throws IOException {
		byte[] data = documento.getData();
		
    	response.setContentType(SOLICITUD_XML_MIME_TYPE);
		response.setContentLength(data.length);
		
	    OutputStream output = response.getOutputStream();
	    try {
			output.write(data);
	    }
	    finally {
	    	output.close();
	    }
	}
}
