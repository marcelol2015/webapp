package com.acepta.ecuador.servlet;

import java.util.ArrayList;
import java.util.TimerTask;

public class AceptaServletTimer {
	private static class AceptaTimerTask {
		private Thread timerThread;
		private TimerTask timerTask;
		private long timerPeriod;
		private boolean running;
		
		public AceptaTimerTask(TimerTask timerTask, long timerPeriod) {
			setTimerTask(timerTask);
			setTimerPeriod(timerPeriod);
			setTimerThread(createTimerThread());
		}

		public void start() {
			setRunning(true);
			getTimerThread().start();
		}
		
		public void stop() {
			setRunning(false);
			try {
				getTimerThread().join();
			}
			catch (InterruptedException exception) {
			}
		}
		
		private Thread getTimerThread() {
			return timerThread;
		}

		private void setTimerThread(Thread timerThread) {
			this.timerThread = timerThread;
		}

		private TimerTask getTimerTask() {
			return timerTask;
		}

		private void setTimerTask(TimerTask timerTask) {
			this.timerTask = timerTask;
		}

		private long getTimerPeriod() {
			return timerPeriod;
		}

		private void setTimerPeriod(long timerPeriod) {
			this.timerPeriod = timerPeriod;
		}
		
		private boolean isRunning() {
			return running;
		}

		private void setRunning(boolean running) {
			this.running = running;
		}

		private Thread createTimerThread() {
			return new Thread(new Runnable() {
				public void run() {
					while (isRunning()) {
						if (getTimerTask() != null)
							getTimerTask().run();
						snooze();
					}
				}
				
				private void snooze() {
					final long scheduledTime = getCurrentTime() + getTimerPeriod();

					while (isRunning()) {
						if (getCurrentTime() >= scheduledTime)
							break;
						try {
							Thread.sleep(1000L);
						}
						catch (InterruptedException exception) {
						}
					}
				}

				private long getCurrentTime() {
					return System.currentTimeMillis();
				}
			});
		}
	}
	
	private ArrayList<AceptaTimerTask> timerTasks;

	public AceptaServletTimer() {
		setTimerTasks(new ArrayList<AceptaTimerTask>());
	}

	public void schedule(TimerTask timerTask, long timerPeriod) {
		getTimerTasks().add(new AceptaTimerTask(timerTask, timerPeriod));
	}
	
	public void start() {
		for (AceptaTimerTask task : getTimerTasks())
			task.start();
	}
	
	public void stop() {
		for (AceptaTimerTask task : getTimerTasks())
			task.stop();
	}

	private ArrayList<AceptaTimerTask> getTimerTasks() {
		return timerTasks;
	}

	private void setTimerTasks(ArrayList<AceptaTimerTask> timerTasks) {
		this.timerTasks = timerTasks;
	}
}
