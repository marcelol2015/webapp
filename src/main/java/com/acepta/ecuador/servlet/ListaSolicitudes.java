package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.Solicitud;
import com.acepta.ecuador.persistence.SolicitudPersona;
import org.apache.velocity.VelocityContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;

/**
 * Modulo:
 * 	???
 * 
 * Descripcion:
 * 	Lista de Solicitudes pendientes.
 *
 */
public class ListaSolicitudes extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	private static final String VM_LISTA_SOLICITUDES = "/META-INF/vm/ListaSolicitudes.vm";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Collection<SolicitudPersona> solicitudes = findSolicitudesPendientes();
			
			sendListadoSolicitudes(request, response, solicitudes);
			
			log("[Acepta] Listado de Solicitudes pendientes enviado");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al enviar listado de Solicitudes pendientes", exception);
		}
	}

	private void sendListadoSolicitudes(HttpServletRequest request, HttpServletResponse response, Collection<SolicitudPersona> solicitudes) throws ServletException, IOException {
		VelocityContext context = new VelocityContext();
		
		context.put("fecha", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		context.put("solicitudes", solicitudes);

		mergeVelocityTemplate("text/html", VM_LISTA_SOLICITUDES, context, response);
	}

	private Collection<SolicitudPersona> findSolicitudesPendientes() throws ServletException {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			return personaBean.buscarSolicitudesPersonaConEstado(Solicitud.Estado.PENDIENTE);
		}
		finally {
			personaBean.close();
		}
	}
}
