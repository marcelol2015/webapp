package com.acepta.ecuador.servlet.data;

import com.acepta.ecuador.persistence.SolicitudPersona;
import com.acepta.ecuador.security.util.BASE64;

import java.text.SimpleDateFormat;


public class CertificadoPersonaACPEV1Adapter implements CertificadoPersona {
	private final SolicitudPersona solicitud;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public CertificadoPersonaACPEV1Adapter(SolicitudPersona solicitud) {
		this.solicitud = solicitud;
	}

	public String getAutoridad() {
		return ACPEV1;
	}

	public String getClase() {
		if (solicitud.getCertificado().getEmisor().isDigital()){
			return DIGITAL;
		}
		else if(solicitud.getCertificado().getEmisor().getNombre().equalsIgnoreCase("Tributaria-V1")){
			return TRIBUTARIA;
		}else{
			return ELECTRONICA;
		}
	}

	public String getCodigoSolicitud() {
		return solicitud.getCodigo();
	}

	public String getEstado() {
		return (solicitud.getCertificado().isVigente() ? VIGENTE : (solicitud.getCertificado().isExpirado() ? EXPIRADO
				: (solicitud.getCertificado().isSuspendido() ? SUSPENDIDO : REVOCADO)));
	}

	public String getNumeroSerie() {
		return String.valueOf(this.solicitud.getCertificado().getNumeroSerie());
	}

	public String getDni() {
		return solicitud.getTitular().getDni();
	}

	public String getValidoDesde() {
		return dateFormat.format(solicitud.getCertificado().getValidoDesde());
	}

	public String getValidoHasta() {
		return dateFormat.format(solicitud.getCertificado().getValidoHasta());
	}

	public String getNombre() {
		return solicitud.getCertificado().getSujeto();
	}

	public String getNumeroSolicitud() {
		return solicitud.getNumero();
	}

	public String getCertificado() {
		return BASE64.encode(solicitud.getCertificado().getData());
	}
}
