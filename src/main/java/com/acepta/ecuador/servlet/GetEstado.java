package com.acepta.ecuador.servlet;

import com.acepta.ecuador.bean.SolicitudPersonaBean;
import com.acepta.ecuador.persistence.SolicitudPersona;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Modulo:
 * 	Aplicacion Operador Validacion.
 * 
 * Descripcion:
 * 	Devuelve estado de una Solicitud de Persona.
 * 
 */
public class GetEstado extends AceptaServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codigo = request.getParameter("CodigoSolicitud");
		
		try {
			SolicitudPersona solicitud = findSolicitudByCodigo(codigo);

			int codigoEstado = getCodigoEstadoFromSolicitud(solicitud);
			
			sendText(response, String.valueOf(codigoEstado));
			
			log("[Acepta] Solicitud No. " + codigo + " esta en estado " + formatearEstado(codigoEstado) + " (Codigo: " + codigoEstado + ")");
		}
		catch (Exception exception) {
			sendText(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
			
			log("[Acepta] Falla al obtener el estado de la Solicitud No. " + codigo, exception);
		}
	}

	private SolicitudPersona findSolicitudByCodigo(String codigo) {
		SolicitudPersonaBean personaBean = getBeanFactory().createSolicitudPersonaBean();
		try {
			SolicitudPersona solicitud = personaBean.buscarSolicitudPersonaConCodigo(codigo);
	
			return solicitud;
		}
		finally {
			personaBean.close();
		}
	}	

	private int getCodigoEstadoFromSolicitud(SolicitudPersona solicitud) {
		if (solicitud.isPendiente())
			return SOLICITUD_ESTADO_REGISTRADO;

		if (solicitud.isAprobada())
			return SOLICITUD_ESTADO_VALIDADO;

		if (solicitud.isRechazada())
			return SOLICITUD_ESTADO_RECHAZADO;

		if (solicitud.isCerrada()) {
			if (solicitud.getCertificado().isValido())
				return SOLICITUD_ESTADO_EMITIDO;

			if (solicitud.getCertificado().isSuspendido())
				return SOLICITUD_ESTADO_SUSPENDIDO;

			if (solicitud.getCertificado().isRevocado())
				return SOLICITUD_ESTADO_REVOCADO;
		}

		return SOLICITUD_ESTADO_DESCONOCIDO;
	}	
}
